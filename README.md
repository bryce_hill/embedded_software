README

## Created Sept. 1, 2016
## Embedded Firmware Code in c (ECEN 5013 Class Repository)
## Authors:

    - Bryce Hill
    - Maurice Woods

### ------- Purpose  : -------
This repository was created as a class repository for ECEN 5013 - Embedded
Software Essentials. It contains firmware for the FRDM KL25Z and BeagleBone
Black boards. GNU Make was used as the build system and an architecturally
independent makefile is included.

### ------- Directory : -------

#### HW
Assorted homework directories

#### Project
Main project files

- Includes firmware for FRDM KL25Z and BeagleBoneBlack
- Architecturally independent Makefile

#### hellowWorld
Contains some initial testing files