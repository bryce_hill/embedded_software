/*
 * Written: 9/9/25
 *      by: Bryce Hill & Mo Woods
 * 
 * Union/Structure/Bit Field declaration
 *  - Based on ADC14CTL0 resgister provided in class
 *  
 */

#include <stdint.h>
#include <stdio.h>

typedef union{
  struct{
    uint32_t ADC14SC:1;
    uint32_t ADC14ENC:1;
    uint32_t _reserved0:2;
    uint32_t ADC14ON:1;
    uint32_t _reserved1:2;
    uint32_t ADC14MSC:1;
    uint32_t ADC14SHt0x:4;
    uint32_t ADC14SHT1x:4;
    uint32_t ADC14BUSY:1;
    uint32_t ADC14CONSEQx:2;
    uint32_t ADC14SSELx:3;
    uint32_t ADC14DIVx:1;
    uint32_t ADV14DIVx:1;
    uint32_t ADC14ISSH:1;
    uint32_t ADC14SHP:1;
    uint32_t ADC14SHSx:3;
    uint32_t ADC14PDIV:2;
  } b;
  uint32_t w;
}Type;

int main(){
  Type data; 	// Do not need "union" declaration because typedef 
  printf("Memory size occupied by union = %lu\n", sizeof(data));
 
  // Access individual bit field 
  data.b.ADC14SSELx = 3;
  printf("ADC14SSELx = %d\n", data.b.ADC14SSELx);  

  // Access entire structure by pointer
  uint32_t *aptr = &data.w;  
  printf("Pointer to structure = %lu\n", (unsigned long)aptr);
  
  return 0;
}		
