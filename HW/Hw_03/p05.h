typedef enum {FULL,EMPTY,OVERFLOW,NORMAL}bufferstates_t;

uint8_t* createBuffer(uint8_t size);
bufferstates_t bufferStatus(void);
bufferstates_t bufferPush(uint8_t* src, uint8_t length);
bufferstates_t bufferPop(uint8_t* dest, uint8_t length);
