#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "p05.h"

uint8_t bufferSize = 127;
uint8_t* buffHead;
uint8_t* buffTail;
uint8_t* buffMin;
uint8_t* buffMax;
uint8_t buffCounter;


uint8_t main(void){
  buffHead = buffTail = buffMin = buffMax = createBuffer(bufferSize);
  createBuffer(bufferSize);
  return(0);
}


uint8_t* createBuffer(uint8_t size){
    uint8_t* bufferAddress = malloc(size);
    return(bufferAddress);
}


bufferstates_t bufferStatus(void){
  bufferstates_t status;
  if(buffCounter==0){
    status=EMPTY;
    printf("Buffer EMPTY\n");
  }  
  else if(buffCounter==bufferSize){
    status=FULL;
    printf("Buffer FULL\n");
  }
  else if(buffCounter>bufferSize){
    status=OVERFLOW;
    printf("Buffer OVERFLOWED\n");
  } 
  else{
    status=NORMAL;
    printf("Buffer NORMAL\n");
  } 
  return(status);
}


bufferstates_t bufferPush(uint8_t* src, uint8_t length){
  bufferstates_t status = bufferStatus();
  if(status==FULL || status==OVERFLOW){
    return(status);
  }
  for(uint8_t i=0;i=length;i++){
    if(buffHead==buffMax){
      buffHead=buffMin;
    }
    *buffHead=*src;
    src++;
    buffHead++;
    buffCounter++;
  }
  return(status);
}


bufferstates_t bufferPop(uint8_t *dest, uint8_t length){
  bufferstates_t status = bufferStatus();
  if(status==EMPTY){
    return(status);
  }
  for(uint8_t i=0;i=length;i++){
    if(buffHead==buffMin){
      buffHead=buffMax;
    }
    *dest=*buffHead;
    dest++;
    buffHead--;
    buffCounter--;
  }
  return(status);
}
