	.cpu cortex-m0plus
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.code	16
	.file	"p02b.c"
	.text
	.align	2
	.global	main
	.code	16
	.thumb_func
	.type	main, %function
main:
	push	{r7, lr}
	ldr	r7, .L5
	add	sp, sp, r7
	add	r7, sp, #0
	mov	r3, r7
	mov	r2, #200
	lsl	r2, r2, #2
	add	r2, r7, r2
	str	r3, [r2]
	mov	r3, #200
	lsl	r3, r3, #1
	add	r3, r7, r3
	mov	r2, #201
	lsl	r2, r2, #2
	add	r2, r7, r2
	str	r3, [r2]
	b	.L2
.L3:
	mov	r3, #200
	lsl	r3, r3, #2
	add	r3, r7, r3
	ldr	r3, [r3]
	ldr	r3, [r3]
	add	r2, r3, #2
	mov	r3, #201
	lsl	r3, r3, #2
	add	r3, r7, r3
	ldr	r3, [r3]
	str	r2, [r3]
	mov	r3, #200
	lsl	r3, r3, #2
	add	r3, r7, r3
	ldr	r3, [r3]
	add	r3, r3, #4
	mov	r2, #200
	lsl	r2, r2, #2
	add	r2, r7, r2
	str	r3, [r2]
	mov	r3, #201
	lsl	r3, r3, #2
	add	r3, r7, r3
	ldr	r3, [r3]
	add	r3, r3, #4
	mov	r2, #201
	lsl	r2, r2, #2
	add	r2, r7, r2
	str	r3, [r2]
.L2:
	mov	r3, #200
	lsl	r3, r3, #1
	add	r3, r7, r3
	add	r3, r3, #141
	add	r3, r3, #255
	mov	r2, #201
	lsl	r2, r2, #2
	add	r2, r7, r2
	ldr	r2, [r2]
	cmp	r2, r3
	bls	.L3
	mov	r3, #0
	mov	r0, r3
	mov	sp, r7
	mov	r3, #202
	lsl	r3, r3, #2
	add	sp, sp, r3
	@ sp needed
	pop	{r7, pc}
.L6:
	.align	2
.L5:
	.word	-808
	.size	main, .-main
	.ident	"GCC: (GNU Tools for ARM Embedded Processors) 4.9.3 20141119 (release) [ARM/embedded-4_9-branch revision 218278]"
