/* strcpy 
 *    Copies all of the characters from a string into another string
 */

/* strncpy - 
 *    Copies a certain number of characters from a string into another string
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>

uint8_t src1[] = "Maurice";
uint8_t dest1[] = "Woods";
uint8_t src2[] = "Bryce";
uint8_t dest2[] = "Hill";
uint8_t src3[] = "Maurice";
uint8_t dest3[] = "Woods";
uint8_t src4[] = "Bryce";
uint8_t dest4[] = "Hill";
uint8_t n = sizeof(dest3)-1;

int main (void){
  printf("We have four strings:\nsrc1 ='%s'\ndest1='%s'\nsrc2 ='%s'\ndest2='%s'\n\n",src1,dest1,src2,dest2);
  strcpy(dest1,src1);
  printf("If we use strcpy() to copy src1 to dest1, our strings become:\nsrc1 ='%s'\ndest1='%s'\nsrc2 ='%s'\ndest2='%s'\n\n...\n\n",src1,dest1,src2,dest2);

  printf("Notice that the copy of a string that is %d characters long (including an 'invisible' null character) into a string that is %d characters long, we end up overwriting into our third string (src2)! BAD strcpy! No biscuit!\n\n...\n\n",(uint8_t)sizeof(src1),(uint8_t)sizeof(dest1));
  
  printf("Start over with the same four strings: \nsrc3 ='%s'\ndest3='%s'\nsrc4 ='%s'\ndest4='%s'\n\n",src3,dest3,src4,dest4);
  strncpy(dest3,src3,n);
  printf("Now, if we use strncpy() with a 'num' parameter n = sizeof(dest1)-1 =%d (where we subtract one to avoid overwriting the null string terminator at the end of src3), that same operation copies the first 'n' characters of src3 into dest3, avoiding the issue of overflowing into src4:\nsrc3 ='%s'\ndest3='%s'\nsrc4 ='%s'\ndest4='%s'\n\n",n,src3,dest3,src4,dest4);

  return(0);
}

/* 
 * This is particularly useful if you had a buffer that you did not want to overflow
 */
