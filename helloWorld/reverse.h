#ifndef _reverse_h_
#define _reverse_h_

/*
 * Created on: Sept 13, 2016
 *     Author: Bryce Hill
 * 	  For: ECEN 5013 HW 3
 */


# include <stdio.h>

char reverse(char* str, int length);

#endif
