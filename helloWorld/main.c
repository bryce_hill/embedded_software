#include <stdio.h>
#include "reverse.h"

int main(){
  printf("Hello World\n");

  char arr[] = "This is a string";
  char* point = arr;
  int len = 16;
  reverse(point, len); 

  char arr2[18] = "some NUMmbers12345";
  char* point2 = arr2;
  int len2 = 18;
  reverse(point2, len2);

  char arr3[31] = "Does it reverse \n\0\t correctly?";
  char* point3 = arr3;
  int len3 = 31;
  reverse(point3, len3);
	
  return 0;
}

