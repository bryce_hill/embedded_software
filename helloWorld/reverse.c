/*
 * reverse.c
 *
 *  Created on: Aug 31, 2016
 *      Author: Bryce
 *    
 *  Edited for HW2
 */

#include <stdio.h>


/*HW 1 #9
 * - Reverse a char array
 * - If array length = 0 or values contained are not char, return exit code = 1
 * - If all goes well, return 0
 */
char reverse(char* str, int length){
	if(length == 0){
		return 1;
	}
	char* ptr = str;
	char* endptr = str + (char)length - 1; //
//	printf("Endptr declared as:\n %d\n", *endptr);
	char temp = 0; 		// Hold data value

	printf("Original Array:\n");
	int ii = 0;
	for(ii; ii < length; ii++){
		printf("%c ", *str);
		str++;
	}
	str = ptr;

	while(ptr < endptr){
		// Check that data is of size char
		if(sizeof(*ptr)/sizeof(char)!=1){
			printf("\nData bad\n");
			return 1;
		}
		temp = *ptr;	// Store value
		*ptr = *endptr;	// Replace with end value
		*endptr = temp;	// Replace end value with first value
		ptr++;			// increment the array pointer
		endptr--;		// Decrement end pointer
	}
	printf("\nReversed Array:\n");
	int j = 0;
	for(j; j < length; j++){
		printf("%c ", *str);
		str++;
	}
	printf("\n\n");
	return 0;
}


/* HW 1 #7
 * - Check data type sizes on host machine
 */
/*
void dataType(){
	int sizeChar = sizeof(char);
	int sizeInt = sizeof(int);
	int sizeFloat = sizeof(float);
	int sizeDouble = sizeof(double);
	int sizeShort = sizeof(short);
	int sizeLong = sizeof(long);
	int sizeLongInt = sizeof(long int);
	int sizelongLong = sizeof(long long);
	int sizeint8 = sizeof(int8_t);
	int sizeofuint8 = sizeof(uint8_t);
	int sizeofuint16 = sizeof(uint16_t);
	int uint32 = sizeof(uint32_t);
	int cptr = sizeof(char*);
	int iptr = sizeof(int*);
	int fptr = sizeof(float*);
	int vptr = sizeof(void*);
	int i8ptr = sizeof(int8_t*);
	int i16ptr = sizeof(int16_t*);
	int i32ptr = sizeof(int32_t*);

	printf("Char = %d\nInt= %d\nFloat = %d\nDouble = %d\nShort = %d\n", sizeChar, sizeInt, sizeFloat, sizeDouble, sizeShort);
	printf("Long = %d\nLong Int = %d\nLong Long = %d\nInt8_t = %d\n", sizeLong, sizeLongInt, sizelongLong, sizeint8);
	printf("uint8_t = %d\nuint16_t = %d\nuint32_t = %d\n", sizeofuint8, sizeofuint16, uint32);
	printf("char* = %d\nint* = %d\nfloat* = %d\nvoid* = %d\nint8* = %d\nint16* = %d\nint32* = %d\n", cptr, iptr, fptr, vptr, i8ptr, i16ptr, i32ptr);
}

/* HW 1 # 6
 * - Verify result found in problem 5
 */
/*
void ptrTable(){
	unsigned char table[8] = {0xFE, 0x34, 0x8C, 0x40, 0x61, 0x28, 0x23, 0x40};
// 1.
	unsigned char *ptr = table;
// 2.
	*ptr = 0xF1 & 127;
// 3.
	ptr++;
// 4.
	*ptr += 17;
// 5.
	ptr += 2;
//6.
	*ptr = 15 % 4;
//7
	ptr--;
//8
	*ptr >>=4;
//9
	ptr = &table[5];
//10
	*ptr = (1<<5)|(4<<2);
//11
	ptr++;
	ptr++;
	*ptr = 22;

	ptr = table;
	for(int i = 0; i < 8; i++){
		printf("%x, ", table[i]);
	}

}
*/
/*
int main() {

	printf("-- Call reverse fcn -- \n");
	char array[10] = {12, 1, 2, 3, 4, 5};
	int len = sizeof(array);
	char* point = array;
	char result = reverse(point, len);

	printf("\n\n-- Call data type fcn -- \n");
	dataType();

	printf("\n-- Call table fcn (0x__)--\n");
	ptrTable();
}*/
