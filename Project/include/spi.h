/*
 * File: spi.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 06-Nov-2016
 *
 * Resources: https://community.nxp.com/thread/311824
 */

#ifndef __SPI_H_
#define __SPI_H_

#include <stdint.h>
#include "init.h"
#include <stdio.h>
#include "circBuffer.h"
#include "log.h"
#include "macro.h"
#include "timer.h"
// #include "nRF24L01.h"

#if arch == frdm
    #include "MKL25Z4.h"
    #include "system_MKL25Z4.h"
#endif

// Define the global variable
uint8_t csn_hold;
uint8_t spi0_rxflag;

#define CSN 0x400

/*
* Function: initialize_spi0
*
* Description:
*   Configures the control registers responsible for initializing UART0
* Parameters:
*   1: n/a
* Returns:
*   (int8_t) -  Error indicating move failure(1) or success(0)
*/
void initialize_spi0(void);

void write_spi0(uint8_t *spimsg);

/*
 * Function: put_spi0
 *
 * Description:
 *   Transmit a byte via UART0
 * Parameters:
 *   1: (uint8_t) byte
 * Returns:
 *   (int8_t) -  Error indicating move failure(1) or success(0)
 */
void put_spi0(uint8_t byte);

/*
 * Function: chipSelect
 *
 * Description:
 *   Set or Clear chip select
 * Parameters:
 *   1: (CSE_t) action - Set or Clear
 * Returns:
 *   (void) 
 */
void chipSelect(CSE_t action);

#endif
