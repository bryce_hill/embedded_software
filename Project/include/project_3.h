/*
 * File: project_3.h
 * Author: Maurice Woods and Bryce Hill
 *
 * - This file holds project 3 report related unit tests and output for write up
 *
 * Created on: 07-Nov-2016
 */

#ifndef __PROJECT_3_H_
#define __PROJECT_3_H_

#include "circBuffer.h"
#include "uart.h"
#include "message.h"
#include "log.h"
#include "dma.h"
#include "memory.h"
#include "timer.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Function: my_project_3_report
 *
 * Description:
 *   Rigorously tests the function of project_3 code and outputs results
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void my_project_3_report(void);

#endif
