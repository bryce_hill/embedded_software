/*
 * File: log.h
 * Author: Maurice Woods and Bryce Hill
 * Created on: 22-Oct-2016
 */

#ifndef __LOG_H__
#define __LOG_H__

#include "uart.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Function: LOG0
 *
 * Description:
 *   Prints string, via UART of printf depending on Architecture
 * Parameters:
 *   1: (uint8_t *)data[] - Pointer to array of data to print
 * Returns:
 *	 void
 */
void LOG0(uint8_t* data);

/*
 * Function: LOG8
 *
 * Description:
 *   Prints int array, via UART of printf depending on Architecture
 * Parameters:
 *   1: (uint8_t *)data - Pointer to array of data to print
 *   2: (uint8_t)length - length of data to print
 * Returns:
 *
 */
void LOG8(uint8_t* data, uint8_t length);

/*
 * Function: LOG32
 *
 * Description:
 *   Prints int array, via UART of printf depending on Architecture
 * Parameters:
 *   1: (uint8_t *)data - Pointer to array of data to print
 *   2: (uint8_t)length - length of data to print
 * Returns:
 *
 */
void LOG32(uint32_t* data, uint32_t length);

#endif
