/*
 * File: init.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 16-Sept-2016
 */

 #ifndef __INIT_H__
 #define __INIT_H__

 #include "macro.h"
 #include "timer.h"
 #include "peripherals.h"
 #include "uart.h"
 #include <stdio.h>
 #include <stdint.h>
 // #include "isr.h"
 #include "spi.h"
 #include "dma.h"
 #include "log.h"
 #include "nRF24L01.h"


/*
 * Function: initialize_ARCH
 *
 * Description:
 *   Configures all registers for selected architecture
 * Parameters:
 *   1: n/a
 * Returns:
 *   n/a
 */
uint8_t initialize_ARCH(void);

#endif
