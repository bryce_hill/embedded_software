/*
 * File: dma.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: Oct-2016
 */

#ifndef __MY_DMA_H__
#define __MY_DMA_H__

#include "macro.h"
#include <stdint.h>
#include "uart.h"
#include "stdio.h"
#include "timer.h"

#if arch == frdm
	#include "MKL25Z4.h"
#endif

my_timer_t dma_timer;

/*
 * Function: dma_config_0
 *
 * Description:
 *   Configures the DMA0 module on the FRDM KL25Z board
 * Parameters:
 *   Void
 * Returns:
 *   Void
 */
void dma_config_0(void);

/*
 * Function: dma_start
 *
 * Description:
 *   Initiates DMA transfer
 * Parameters:
 *   volatile uint8_t src - source location for data
 *   volatile uint8_t dest - destination location for data
 *   uint16_t length - Length fo data to be transferred
 * Returns:
 *   Void
 */
void dma_start(volatile uint8_t * src,volatile uint8_t * dest, uint16_t length);

/*
 * Function: dma_task
 *
 * Description:
 *   This interfaces DMA functions with the task architecture used in the main loop
 * Parameters:
 *   Void
 * Returns:
 *   Void
 */
void dma_task(void);

/*
 * Function: dma_memmove
 *
 * Description:
 *   Is called by dma_start and actually executes the data transfer
 * Parameters:
 *   volatile uint8_t* src - source location for data
 *   volatile uint8_t* dest - destination location for data
 *   uint16_t size - Length fo data to be transferred
 * Returns:
 *   Void
 */
void dma_memmove(volatile uint8_t * source,volatile uint8_t * destination, uint8_t size);

#endif
