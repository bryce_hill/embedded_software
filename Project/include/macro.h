/*
 * File: macro.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 24-Oct-2016
 */

#ifndef _MACRO_H_
#define _MACRO_H_

#include <stdint.h>

// DECLARE some global variables (need to be defined using "extern" in headers of files that wish to use them)
extern uint64_t my_systicks;
extern uint64_t count;
extern uint8_t tpm1Flag;
extern uint8_t init_RF_flag;
extern uint8_t alarm_flag;
extern uint8_t alarm_flag1;
extern uint8_t csn_hold;
extern uint8_t spi0_rxflag;

typedef struct {
	uint64_t wait_timer_1;
	uint8_t wait_rollover_1;
	uint64_t wait_timer_2;
	uint8_t wait_rollover_2;
	uint64_t wait_timer_3;
	uint8_t wait_rollover_3;
}alarm_t;

extern alarm_t wait_timers_list;

typedef enum {CLEAR, SET} CSE_t;

// Todo move to separate header
#define SUCCESS 0
#define FAILURE 1

#define EMPTY 0
#define FULL 1

#define TOGGLE 2
#define ON 1
#define OFF 0

// Define arch macros (these are not caps in keeping with makefile convention)
#define host 0
#define frdm 1
#define bbb 2
#define arch frdm

// Define timer macros
#define PWM OFF 	// Make timers into PWM modules and link to LED pins

// Turn on printf UART settings
#define DEBUG 1

// General GPIO
#define SPI ON

// UART INPUT
// 	1 = command message UART, 2 = LED set via keyboard inputs
#define KEYLED 0
#define CMDS 1
#define MESSAGES KEYLED

// nRF mode
#define TRANSMITTER 0
#define RECIEVER 1
#define NRFMODE TRANSMITTER

// typedef enum{
// 	recieved,
// 	notrecieved
// } MSG_FLAG_t;
// MSG_FLAG_t message = notrecieved;

#endif
