/*
 * File: nRF24L01.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: ~-Sept-2016
 *
 * Resources:
 */

#ifndef __PERIPHERALS_H__
#define __PERIPHERALS_H__

#include <stdint.h>
#include <stdio.h>
#include "macro.h"
#include "circBuffer.h"

// Define some duty cycle contants
#define DUTY_MAX 350
#define DUTY_MED 175
#define DUTY_MIN 0


#if arch == frdm
	#include "MKL25Z4.h"
#endif

typedef enum {RED,YELLOW,GREEN,CYAN,BLUE,VIOLET,ALL} color_t;

#if arch == frdm
	/*
	* Function: initialize_rgbled
	*
	* Description:
	*   Configures FRDM KL25Z Board ports to enable rgb LED
	* Parameters:
	*   void
	* Returns:
	*   VOID
	*/
	uint8_t initialize_rgbled(void);

	/*
	* Function: toggle_led
	*
	* Description:
	*   Toggles a diode of the LED
	* Parameters:
	*   color_t RGB_LED - defines color to toggle
	* Returns:
	*   uint8_t - Success or Failur macro
	*/
	uint8_t toggle_led(color_t RGB_LED);

	/*
	* Function: led_off
	*
	* Description:
	*   Turns off a diode of the LED
	* Parameters:
	*   color_t RGB_LED - defines color to toggle
	* Returns:
	*   uint8_t - Success or Failur macro
	*/
	uint8_t led_off(color_t RGB_LED);

	/*
	* Function: led_on
	*
	* Description:
	*   Turns on a diode of the LED
	* Parameters:
	*   color_t RGB_LED - defines color to toggle
	* Returns:
	*   uint8_t - Success or Failur macro
	*/
	uint8_t led_on(color_t RGB_LED);

	/*
	* Function: user_input_pwm_led
	*
	* Description:
	*   Allows the user to input commands from the terminal to control LED
	* Parameters:
	*   void
	* Returns:
	*   void
	*/
	void user_input_pwm_led(void);
#endif

#endif
