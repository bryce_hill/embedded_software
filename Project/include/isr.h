/*
 * File: isr.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: ~-Sept-2016
 */

#ifndef _ISR_H_
#define _ISR_H_

#include "macro.h"
#include <stdint.h>
#include "peripherals.h"
#include "log.h"
#include "timer.h"
#include "dma.h"
// Include init.h in order to have access to global variables
	// #include "init.h"

#if arch == frdm
	#include "MKL25Z4.h"
#endif

typedef enum {RED_TOGGLE_FLAG,GREEN_TOGGLE_FLAG,BLUE_TOGGLE_FLAG,TX_FLAG,RX_FLAG} isr_flags_t;

// TODO not using these isr flags
// #ifndef my_isr_flags
uint16_t my_isr_flags;
// #endif

// Define the global variables
uint8_t tpm1Flag;
uint64_t my_systicks;
uint64_t count;
uint8_t spi0_rxflag;

// Not actually using
void check_isr_flags(void);

/*
 * Function: TPM0_IRQHandler
 *
 * Description:
 *   Handles interrupts for timer0 module
 * Parameters:
 *   void
 * Returns:
 *	 void
 */
void TPM0_IRQHandler(void);

/*
 * Function: TPM1_IRQHandler
 *
 * Description:
 *   Handles interrupts for timer1 module
 * Parameters:
 *   void
 * Returns:
 *	 void
 */
void TPM1_IRQHandler(void);

/*
 * Function: TPM2_IRQHandler
 *
 * Description:
 *   Handles interrupts for timer2 module
 * Parameters:
 *   void
 * Returns:
 *	 void
 */
void TPM2_IRQHandler(void);

/*
 * Function: DMA0_IRQHandler
 *
 * Description:
 *   Handles interrupts for dma0 module
 * Parameters:
 *   void
 * Returns:
 *	 void
 */
void DMA0_IRQHandler(void);

/*
 * Function: SPI_IRQHandler
 *
 * Description:
 *   Handles interrupts for spi module
 * Parameters:
 *   void
 * Returns:
 *	 void
 */
void SPI_IRQHandler(void);

#endif
