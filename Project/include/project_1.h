/*
 * File: project_1.h
 * Author: Maurice Woods and Bryce Hill
 * Created on: 16-Sept-2016
 */

#ifndef __PROJECT_1__
#define __PROJECT_1__

#include "memory.h"
#include "data.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Function: my_project_1_report
 *
 * Description:
 *   Rigorously tests the function of project_1 code and outputs results
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void my_project_1_report(void);

#endif
