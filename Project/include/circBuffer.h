/*
 * File: circBuffer.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 06-September-2016
 */


#ifndef __CIRBUFFER_H__
#define __CIRBUFFER_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "log.h"
#include "macro.h"
#include "data.h"

typedef enum {BUFF_FULL,BUFF_EMPTY,BUFF_OVERFLOW,BUFF_NORMAL,BUFF_WRAP}bufferstates_t;

typedef struct {
	uint8_t bufferSize;
	uint8_t* buffHead;
	uint8_t* buffTail;
	uint8_t* buffMin;
	uint8_t* buffMax;
	uint8_t buffCounter;
	uint8_t wrap;
	bufferstates_t status;
} CircBuf_t;

CircBuf_t TXbuffer;
CircBuf_t RXbuffer;


/*
 * Function: createBuffer
 *
 * Description:
 *   Creates an allocated block in memory of specified size for buffer
 * Parameters:
 *   1: (uint8_t) size - Specify size of buffer needed
 * Returns:
 *   (int8_t*) - Pointer to array location in memory
 */
CircBuf_t createBuffer(CircBuf_t buff, uint16_t size);

/*
 * Function: bufferStatus
 *
 * Description:
 *   Creates an allocated block in memory of specified size for buffer
 * Parameters:
 *   1: (void) - No inputs
 * Returns:
 *   (enum) bufferstates_t - With indicated state of buffer
 */
CircBuf_t bufferStatus(CircBuf_t buff);

/*
 * Function: bufferPush
 *
 * Description:
 *   Pushes data onto buffer
 * Parameters:
 *   1: (uint8_t) length - Specify size of item to be pushed onto buffer
 *   2: (uint8_t*) src - Point to location of data to be pushed
 * Returns:
 *   (enum) bufferstates_t - State of buffer
 */
CircBuf_t bufferPush(CircBuf_t buff, uint8_t *src, uint8_t length);

/*
 * Function: bufferPop
 *
 * Description:
 *   Pops data off the buffer
 * Parameters:
 *   1: (uint8_t) length - Specify size of item to be popped off of buffer
 *   2: (uint8_t*) src - Point to location of data to be pushed
 * Returns:
 *   (enum) bufferstates_t - State of buffer
 */
CircBuf_t bufferPop(CircBuf_t buff, uint8_t *dest, uint8_t length);

/*
 * Function: bufferPrint
 *
 * Description:
 *   Prints data in buffer to terminal via printf
 * Parameters:
 *   Void
 * Returns:
 *   Void
 */
void bufferPrint(CircBuf_t buff);

/*
 * Function: bufferPLog
 *
 * Description:
 *   Wrapper that interfaces to the LOG layer
 * Parameters:
 *   CircBuf_t: Buffer
 * Returns:
 *   Void
 */
void bufferLog(CircBuf_t buff);

#endif
