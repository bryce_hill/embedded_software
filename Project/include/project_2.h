/*
 * File: project_2.h
 * Author: Maurice Woods and Bryce Hill
 * Created on: 16-Sept-2016
 */

#ifndef __PROJECT_2__
#define __PROJECT_2__

#include "project_1.h"
#include "memory.h"
#include "data.h"
#include "circBuffer.h"
#include "uart.h"
#include "log.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Function: my_project_2_report
 *
 * Description:
 *   Rigorously tests the function of project_2 code and outputs results
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void my_project_2_report(void);

/*
 * Function: itoaUnitTest
 *
 * Description:
 *   Rigorously tests the function of my_itoa()
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void itoaUnitTest(void);

/*
 * Function: ftoaUnitTest
 *
 * Description:
 *   Rigorously tests the function of my_ftoa()
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void ftoaUnitTest(void);

/*
 * Function: circBufUnitTest
 *
 * Description:
 *   Rigorously tests the function of circular buffer code
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
void circBufUnitTest();

/*
 * Function: echo_1
 *
 * Description:
 *   Echoes UART input. Was used in initial UART development
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
uint8_t echo_1(void);

/*
 * Function: echo_3
 *
 * Description:
 *   Echoes UART input. Was used in initial UART development
 * Parameters:
 *   (void)
 * Returns:
 *   (void)
 */
uint8_t echo_3(void);

#endif
