/*
 * File: nRF24L01.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 06-Nov-2016
 *
 * Resources:
 */

#ifndef __NRF24L01_H_
#define __NRF24L01_H_

#include <stdint.h>
#include <stdio.h>
#include "macro.h"
#include "spi.h"
#include "timer.h"

#if arch == frdm
    #include "MKL25Z4.h"
    #include "system_MKL25Z4.h"
#endif

// Define the global variable
uint8_t init_RF_flag;
uint8_t alarm_flag;
uint8_t alarm_flag1;
uint8_t csn_hold;

// nRF24L01 module SPI commands
#define R_REGISTER 0b00000000       //
#define W_REGISTER 0b00100000
#define R_RX_PAYLOAD 0b01100001
#define W_TX_PAYLOAD 0b10100000
#define FLUSH_TX 0b11100001
#define FLUSH_RX 0b11100010
#define ACTIVATE 0b01010000
#define R_RX_PL_WID 0b01100000
#define W_ACK_PAYLOAD 0b10101000
#define W_ACK_PAYLOAD_NO 0b10110000
#define NOP 0b11111111

// nRF24L01 register addresses
#define CONFIG 0x00
#define EN_AA 0x01
#define EN_RXADDR 0x02
#define SETUP_AW 0x03
#define SETUP_RETR 0x04
#define RF_CH 0x05
#define RF_SETUP 0x06
#define STATUS 0x07
#define OBSERVE 0x08
#define CD 0x09
#define RX_ADDR_P0 0x0A
#define RX_ADDR_P1 0x0B
#define RX_ADDR_P2 0x0C
#define RX_ADDR_P3 0x0D
#define RX_ADDR_P4 0x0E
#define RX_ADDR_P5 0x0F
#define TX_ADDR 0x10
#define RX_PW_P0 0x11
#define RX_PW_P1 0x12
#define RX_PW_P2 0x13
#define RX_PW_P3 0x14
#define RX_PW_P4 0x15
#define RX_PW_P5 0x16
#define FIFO_STATUS 0x17

// nRF24L01 custom command definitions
#define NORDIC_POWER_UP 0x1
#define NORDIC_POWER_DOWN 0x0
#define NORDIC_POWER_UP_MASK 0x02

typedef enum{oneMBPS=0, twoMBPS} datarate_t;
// typedef enum{PRIM_RX=1, PWR_UP, CRCO, EN_CRC, MASK_MAX_RT, MASK_TX_DS, MASK_TX_DR} CONFIG_t;
#define PRIM_RX 0x01
#define PWR_UP 0x02
#define CRCO 0x04
#define EN_CRC 0x08
#define MASK_MAX_RT 0x10
#define MASK_TX_DS 0x20
#define MASK_TX_DR 0x40

// Define enum for chip enable pin
// typedef enum {CLEAR, SET} CE_t;
typedef enum {RXMODE, TXMODE} XMIT_t;

/*
* Function: write_nRF24L01
*
* Description:
*   Reads configuration/status register of nRF24L01 module
* Parameters:
*   uint8_t reg - register of nRF module to read
* Returns:
*   VOID
*/
void read_register(uint8_t reg);

void write_register(uint8_t reg, uint8_t val, CSE_t action);

/*
* Function: write_nRF24L01
*
* Description:
*   Write data to nRF24L01 module
* Parameters:
*   1: S
* Returns:
*   VOID
*/
void write_registers(uint8_t *data, uint8_t reg, uint8_t size);

/*
* Function: initialize_nRF24L01
*
* Description:
*   Configures the control registers responsible for initializing UART0
* Parameters:
*   1: n/a
* Returns:
*   VOID
*/
void initialize_nRF24L01(void);

/*
* Function: nRF24L01_task
*
* Description:
*   Wrapper to interface nRF module with task architecture in main
* Parameters:
*   VOID
* Returns:
*   VOID
*/
void nRF24L01_task(void);

/*
* Function: write_payload
*
* Description:
*   Write packet to be transmitted to nRF24L01 module
* Parameters:
*   uint8_t *data - pointer to the data to be transmitted
* 	uint8_t size - size of data to be transmitted
* Returns:
*   VOID
*/
void write_payload(uint8_t *data, uint8_t size);

/*
* Function: write_payload
*
* Description:
*   Unused and incomplete function to incorporate a command message protocol as
* 		defined in the message layer
* Parameters:
*   uint8_t command - dommand code
* Returns:
*   VOID
*/
void write_command(uint8_t command);

/*
 * Function: chipEnable
 *
 * Description:
 *   Set/clear CE pin to enable wireless comm via RF module
 * Parameters:
 *   1: (CE_t)
 * Returns:
 *   void
 */
void chipEnable(CSE_t action);



#endif
