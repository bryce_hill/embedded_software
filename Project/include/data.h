/*
 * File: data.h
 * Author: Maurice Woods and Bryce Hill
 * Created on: 16-Sept-2016
 */

#ifndef __DATA__
#define __DATA__

#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Function: my_itoa
 *
 * Description:
 *   Converts data from a standard integer type to an ASCII string. Handles
 *   signed data
 * Parameters:
 *   1: (uint8_t *)str - Pointer to array to write ASCII data to
 *   2: (uint32_t)data - Data that is to be converted
 * Returns:
 *   (int8_t) numberOresults - Number of characters that were found and converted
 */
uint8_t my_itoa(uint8_t * str, int32_t data);

/*
 * Function: my_atoi
 *
 * Description:
 *   Converts data from an ASCII string to a standard integer type. Handles
 *   signed data
 * Parameters:
 *   1: (uint8_t *)str - Pointer to ASCII sting to be converted
 * Returns:
 *   (int32_t) - Integer number converted from ASCII
 */
int32_t my_atoi(uint8_t * str, uint32_t length, uint32_t base);

/*
 * Function: my_dump_memory
 *
 * Description:
 *   Prints the hex output of bytes given a pointer to a memory location and a
*    length of bytes to print
 * Parameters:
 *   1: (uint8_t *)start - Pointer to...
 *   2: (uint32_t)length -
 * Returns:
 *   (void)
 */
void my_dump_memory(uint8_t * start, uint32_t length);

/*
 * Function: my_big_to_little
 *
 * Description:
 *   Converts data types in memory from big endian to little endian
 * Parameters:
 *   1: (uint32_t)data - Data that is to be flipped
 * Returns:
 *   (int32_t) - Flipped data
 */
uint32_t my_big_to_little(uint32_t data);

/*
 * Function: my_little_to_big
 *
 * Description:
 *   Converts data types in memory from little endian to big endian
 * Parameters:
 *   1: (uint32_t)data - Data that is to be flipped
 * Returns:
 *   (int32_t) - Flipped data
 */
uint32_t my_little_to_big(uint32_t data);

/*
 * Function: my_ftoa
 *
 * Description:
 * 	 Converts float data type to
 * Parameters:
 *   1:
 * Returns:
 *
 */
int32_t * my_ftoa(float data, int32_t* result, uint8_t precision);
#endif
