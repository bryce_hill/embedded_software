#ifndef __TIMER_H__
#define __TIMER_H__

#include "macro.h"
#include <stdint.h>
#include "log.h"
// Include init.h in order to have access to global variables
	// #include "init.h"

#if arch == frdm
	#include "MKL25Z4.h"
#endif

// Define the global variable
uint64_t my_systicks;
alarm_t wait_timers_list;

#define SYSTICKS_OV 0xFFFFFFFFFFFFFFFF

typedef struct {
  uint8_t timer_active;
  uint64_t start_time;
  uint16_t overflow_count;
  uint64_t stop_time;
} my_timer_t;

/*
 * Function: timer_init
 *
 * Description:
 *   Initialize tpm timer modules on the FRDM KL25z board
 * Parameters:
 *   void
 * Returns:
 *   (int8_t) -  Should be void
 */
uint8_t timer_init(void);

/*
 * Function: create_timer
 *
 * Description:
 *   Create a timer using the processor cycles and frequency to time
 * Parameters:
 *   my_timer_t timer - Timer struct
 * Returns:
 *   (my_timer_t) -  Updated timer struct
 */
my_timer_t create_timer(my_timer_t timer);

/*
 * Function: restart_timer
 *
 * Description:
 *   Restarts the timer
 * Parameters:
 *   my_timer_t timer - Timer struct
 * Returns:
 *   (my_timer_t) -  Updated timer struct
 */
my_timer_t restart_timer(my_timer_t timer);

/*
 * Function: resume_timer
 *
 * Description:
 *   Resumes the timer
 * Parameters:
 *   my_timer_t timer - Timer struct
 * Returns:
 *   (my_timer_t) -  Updated timer struct
 */
my_timer_t resume_timer(my_timer_t timer);

/*
 * Function: stop_timer
 *
 * Description:
 *   Stops the timer
 * Parameters:
 *   my_timer_t timer - Timer struct
 * Returns:
 *   (my_timer_t) -  Updated timer struct
 */
my_timer_t stop_timer(my_timer_t timer);

/*
 * Function: delay_timer
 *
 * Description:
 *   Delays the timer
 * Parameters:
 *   my_timer_t timer - Timer struct
 * Returns:
 *   Void
 */
void delay_timer(uint16_t delay_time_us);

/*
 * Function: my_delay
 *
 * Description:
 *   Blocking delay of processing
 * Parameters:
 *   uint16_t delay_time - Time in us to delay
 * Returns:
 *   void
 */
void my_delay(uint16_t delay_time);

/*
 * Function: delay_task
 *
 * Description:
 *   Functionally a trigger and alarm for non-blocking delays which uses the
 * 		task architecture in main
 * Parameters:
 *   void
 * Returns:
 *   void
 */
uint8_t delay_task(void);

/*
 * Function: get_systicks
 *
 * Description:
 *   Gets the current value of the sys_tick variable
 * Parameters:
 *   void
 * Returns:
 *   uint64_t - Current value of system ticks
 */
uint64_t get_systicks(void);

/*
 * Function: us_to_ticks
 *
 * Description:
 *   Converts us to equivalent system ticks
 * Parameters:
 *   uint16_t ms - Time in us to convert
 * Returns:
 *   void
 */
uint64_t us_to_ticks(uint16_t ms);

/*
 * Function: diff_systicks
 *
 * Description:
 *   Calculates how many ticks have passed
 * Parameters:
 *   uint16_t mstart - Starting number of systicks
 *	 uint16_t mend - Ending number of systicks
 * Returns:
 *   uint64_t - Difference between end and start
 */
uint64_t diff_systicks(uint64_t mstart, uint64_t mend);

/*
 * Function: sec_st
 *
 * Description:
 *   In place if a conversion factor needs to be used
 * Parameters:
 *   uint64_t my_systicks - Systicks to convert
 * Returns:
 *   Float - Resulting conversion
 */
float sec_st(uint64_t my_systicks);

#endif
