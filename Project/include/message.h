/*
 * File: message.h
 * Author: Maurice Woods and Bryce Hill
 *
 * - This is the hardware abstraction layer that should handle all interdevice
 *	 communication
 *
 * Created on: 05-Nov-2016
 */

#ifndef __MESSAGE_H_
#define __MESSAGE_H_

#include <stdio.h>
#include <stdint.h>
#include "macro.h"
#include "circBuffer.h"
#include "log.h"
#include "peripherals.h"

typedef enum{
	FRDM_LED_CONTROL, print
} Cmds_t;

typedef struct CI_Msg_t{
	Cmds_t command;
	uint8_t length;
	uint8_t data[100]; 	// May need to revisit maximum data size here
	uint8_t checksum;	// Error detect checksum
} CI_Msg;

CircBuf_t Instantiate_Struct(CircBuf_t RXbuffer);

/*
* Function: Decode_CI_Msg()
*
* Description:
*   Determines which message type has been recieved and calls the appropriate
*	handler function
* Parameters:
*   1: (CI_Msg_t) - Pass in a pointer to the message structure
* 	2: (uint8_t) - Pass in the # of elements in the RX buffer, used to wait until
* 				   all data has been recieved to avoid missing data
* Returns:
*   () -
*/
void Decode_CI_Msg(struct CI_Msg_t *msg);

/*
* Function: FRDM_LED_CONTROL()
*
* Description:
*   Sets, clears or toggles RED, GREEN or BLUE diodes based on command input
*		- 03007 red off
*		- 03017 red on
*		- 03027 red toggle
*		- 03207 green off
*		- 03217 green on
*		- 03227 green toggle
* Parameters:
*   1: (CI_Msg_t) - Pass in a pointer to the message structure
* Returns:
*   () -
*/
void FRDM_LED_CONTROL_FUNC(struct CI_Msg_t *msg);

/*
* Function: PRINT_COMMAND()
*
* Description:
*   Prints out whatever message has been put in the data secion
* 	- functions like echo
* 	- 16999999
* Parameters:
*   1: (CI_Msg_t) - Pass in a pointer to the message structure
* Returns:
*   () -
*/
void PRINT_COMMAND(struct CI_Msg_t *msg);

/*
* Function: CMD_task()
*
* Description:
*   Handles user input via UART
* Parameters:
*   1: (void)
* Returns:
*   (void)
*/
void cmd_task(void);
#endif
