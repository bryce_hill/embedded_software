/*
 * File: memory.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 16-Sept-2016
 */

#ifndef __MEMORY__
#define __MEMORY__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "macro.h"
#include "uart.h"
#include "timer.h"
#include "log.h"
#include "data.h"

/*
 * Function: my_memmove
 *
 * Description:
 *   Moves data of specified length from one place to another
 * Parameters:
 *   1: (uint8_t *)src   - Pointer pointing to top of the source data
 *   2: (uint8_t *)dest  - Pointer pointing to top of the destination for the data
 *   3: (uint32_t)length - Length of the data to be moved in bytes
 * Returns:
 *   (int8_t) -  Error indicating move failure(1) or success(0)
 */
uint8_t my_memmove(uint8_t * src, uint8_t * dest, uint32_t length);

/*
 * Function: my_memzero
 *
 * Description:
 *   Zero out all memory in a range
 * Parameters:
 *   1: (uint8_t *)src   - Pointer pointing to top of the source data
 *   2: (uint32_t)length - Length of the data to be zeroed in bytes
 * Returns:
 *   (int8_t) -  Error indicating zero failure(1) or success(0)
 */
uint8_t my_memzero(uint8_t * src, uint32_t length);

/*
 * Function: my_reverse
 *
 * Description:
 *   Reverse the contents of a range of memory
 * Parameters:
 *   1: (uint8_t *)src   - Pointer pointing to top of the source data
 *   2: (uint32_t)length - Length of the data to be reversed in bytes
 * Returns:
 *   (int8_t) -  Error indicating reversal failure(1) or success(0)
 */
uint8_t my_reverse(uint8_t * src, uint32_t length);

/*
 * Function: mymemmove_profiler
 *
 * Description:
 *   Profiles memmove performance
 * Parameters:
 *   1: (uint8_t *)src   - Pointer pointing to top of the source data
 *   2: (uint8_t *)dest  - Pointer pointing to top of the destination for the data
 *   3: (uint32_t)length - Length of the data to be moved in bytes
 * Returns:
 *   void
 */
void mymemmove_profiler(uint8_t * src, uint8_t * dest, uint32_t length);

#endif
