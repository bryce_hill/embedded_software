/*
 * File: uart.h
 * Author: Maurice Woods and Bryce Hill
 *
 * Created on: 05-Oct-2016
 */

#ifndef __UART_H_
#define __UART_H_

#include <stdint.h>
// #include "init.h"
#include <stdio.h>
#include "circBuffer.h"
#include "log.h"
#include "macro.h"


#if arch == frdm
    #include "MKL25Z4.h"
    #include "system_MKL25Z4.h"
#endif

/*
* Function: initialize_uart0
*
* Description:
*   Configures the control registers responsible for initializing UART0
* Parameters:
*   1: n/a
* Returns:
*   (int8_t) -  Error indicating move failure(1) or success(0)
*/
uint8_t initialize_uart0(void);

/*
 * Function: put_uart0
 *
 * Description:
 *   Transmit a byte via UART0
 * Parameters:
 *   1: (uint8_t) byte
 * Returns:
 *   (int8_t) -  Error indicating move failure(1) or success(0)
 */
uint8_t put_uart0(uint8_t byte);

/*
 * Function: get_uart0
 *
 * Description:
 *   Get a byte off RX UART0
 * Parameters:
 *   1: (uint8_t) byte
 * Returns:
 *   (int8_t) - Error indicating move failure(1) or success(0)
 */
uint8_t get_uart0(void);

/*
 * Function: poll_uart0
 *
 * Description:
 *   Check to see if UART0 has been recieved
 * Parameters:
 *   1: void
 * Returns:
 *   (int8_t) -  Error indicating move failure(1) or success(0)
 */
uint8_t poll_uart0(void);

/*
 * Function: UART0_IRQHandler
 *
 * Description:
 *   Handles UART0 RX interrupts.
 * Parameters:
 *   1: void
 * Returns:
 *   void
 */
void UART0_IRQHandler(void);

/*
 * Function: print_uart_settings
 *
 * Description:
 *   Prints out all UART0 settings TODO - This is not really usable now,
 * 		needs to use LOG8
 * Parameters:
 *   1: void
 * Returns:
 *   void
 */
void print_uart_settings(void);

#endif
