# Add every .c file here. patsubst code in main makefile converts each name from .c to .o and adds obj/ and src/ extensions respectively. This allows for .h, .c and .o files to live in separate directories include, src and obj respectively
sources = main.c data.c memory.c project_1.c project_2.c circBuffer.c uart.c init.c peripherals.c log.c message.c project_3.c nRF24L01.c spi.c timer.c
