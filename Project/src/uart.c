#include "uart.h"

// int sysclk = 48000000;
uint32_t baud = 115200;
uint32_t osr = 15;
uint16_t sbr;
uint8_t temp;

uint8_t initialize_uart0(void){

    #if arch == frdm
    	// enable clock gates
      	SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK;						// enable clock gating to port A
        SIM_SCGC4 |= SIM_SCGC4_UART0_MASK; 						// enable clock gating to uart0 module

        // set pins to uart0 rx/tx
        PORTA_PCR1 = PORT_PCR_ISF_MASK | PORT_PCR_MUX(2);
        PORTA_PCR2 = PORT_PCR_ISF_MASK | PORT_PCR_MUX(2);

        // set uart clock to oscillator clock
        SIM_SOPT2 |= SIM_SOPT2_UART0SRC(01);

        UART0_C2  &= ~UART0_C2_TE_MASK & ~UART0_C2_RE_MASK; 	// disable rx and tx for register programmin

        // calculate and set baud rate
        sbr = (uint16_t)((SystemCoreClock)/(baud * osr+1));
        temp = UART0_BDH & ~(UART0_BDH_SBR(0x1F));
        UART0_BDH = temp | UART_BDH_SBR(((sbr & 0x1F00) >> 8)); // set baud rate
        UART0_BDL = (uint8_t)(sbr & UART_BDL_SBR_MASK);
        UART0_C4 |= UART0_C4_OSR(osr);							// set oversampling ratio

        // keep default settings for parity and loopback
        UART0_C1  = 0;
        UART0_C3 |= 0;
        UART0_MA1 = 0;
        UART0_MA2 = 0;
        UART0_S1 |= 0x00;
        UART0_S2  = 0x00;

        UART0_C2 |= UART0_C2_TE_MASK | UART0_C2_RE_MASK;		//re-enable UART

       	//Attach a TX and RX ring buffer to the UART0
	   		// Buffers created in main to avoid access issues

        UART0_C2 |= UART0_C2_TE_MASK | UART0_C2_RE_MASK;		//re-enable UART

    	// Enable UART interupts
        NVIC_EnableIRQ(UART0_IRQn);
    	UART0_C2 |= UART_C2_RIE_MASK;							// Enable RX interrupt
//    	UART0_C2 |= UART_C2_TCIE_MASK;							// TODO - Enable TX interrupt

        #if DEBUG == 1
            uint8_t data[] = "* UART0 Initialized *\n\r";
            LOG8(data, sizeof(data));
        #endif
    #endif

    return(SUCCESS);
}

uint8_t put_uart0(uint8_t byte){
    #if arch == frdm
        while(!(UART0_S1 & UART_S1_TDRE_MASK) && !(UART0_S1 & UART_S1_TC_MASK));
        UART0_D = byte;
        return(SUCCESS);
    #endif

    return(0);
}

uint8_t get_uart0(void){
    #if arch == frdm
        uint8_t byte;
        while(!(UART0_S1 & UART_S1_RDRF_MASK));
        byte = UART0_D;
        return(byte);
    #endif

    return(0);
}

uint8_t poll_uart0(void){
    #if arch == frdm
        if(UART0_S1 & UART_S1_RDRF_MASK){
            return(FULL);
        }
        else{
            return(EMPTY);
        }
    #endif

    return(0);
}

void UART0_IRQHandler (void){
    #if arch == frdm
    uint8_t c = 0;
        if (UART0_S1&UART_S1_RDRF_MASK){
            c = UART0_D;
            uint8_t* cptr = &c;
            RXbuffer = bufferPush(RXbuffer, cptr, 1);
            // toggle_led(GREEN);
            // if ((UART0_S1&UART_S1_TDRE_MASK)||(UART0_S1&UART_S1_TC_MASK)){
            // 	UART0_D  = c;
            // }
        }
    #endif
}

void print_uart_settings(void){
    #if arch == frdm //TODO - This should all use LOG8
        #if DEBUG > 0
    	printf("\n ========== UART0 Settings =========  \n");
    	printf("\n Baud Rate: 0x%X                      \n", baud);
    	printf("\n Over-sampling Ratio: 0x%X            \n", osr);
    	//Data size
    	if(UART0_C1_M_MASK == 0){
    		printf("\n Data size: 8-bit \n");
    	}
    	else{
    		printf("\n Data size: 9-bit \n");
    	}
    	//Everything Parity
    	if(UART0_C1_PE_MASK == 0){
    			printf("\n Parity: None \n");
    	}
    	else{
    		printf("\n Parity: Enabled \n");
    		if(UART0_C1_PT_MASK == 0){
    			printf("\n Parity Type: Even parity \n");
    		}
    		else{
    			printf("\n Parity Type: Odd parity \n");
    		}
    	}
    	//Stop bits
    	if (UART0_BDH_SBNS_MASK == 0){
    		printf("\n Stop Bits: 1 bit \n");
    	}
    	else{
    		printf("\n Stop Bits: 2 bit \n");
    	}
    	printf("\n \n");
    	printf("\n === Raw Register Settings ===        \n");
    	printf("\n UART0_BDH_Register: 0x%X             \n",UART0_BDL);
    	printf("\n UART0_BDL_Register: 0x%X             \n",UART0_BDH);
    	printf("\n UART0_C1_Register:  0x%X             \n",UART0_C1);
    	printf("\n UART0_C2_Register:  0x%X             \n",UART0_C2);
    	printf("\n UART0_C3_Register:  0x%X             \n",UART0_C3);
    	printf("\n UART0_C4_Register:  0x%X             \n",UART0_C4);
    	printf("\n UART0_C5_Register:  0x%X             \n",UART0_C5);
    	printf("\n UART0_MA1_Register: 0x%X             \n",UART0_MA1);
    	printf("\n UART0_MA2_Register: 0x%X             \n",UART0_MA2);
        #endif
    #endif
}
