#include "log.h"

void LOG0(uint8_t* data){
	uint8_t i = 0;
	// uint8_t length = sizeof(data)

	#if arch == frdm
	while(*data != NULL){
		put_uart0(*data);
		data++;
	}
	#endif

	#if arch == host
	while(*data != NULL){
		char dat = (char)*data;
		printf("%c", dat);
		data++;
	}
	#endif
}

void LOG8(uint8_t* data,uint8_t length){
	uint8_t i = 0;
	// uint8_t length = sizeof(data)

	#if arch == frdm
	for(i = 0; i < length; i++){
		put_uart0(*data);
		data++;
	}
	#endif

	#if arch == host
	for(i = 0; i < length; i++){
		char dat = (char)*data;
		printf("%c", dat);
		data++;
	}
	#endif
}

void LOG32(uint32_t* data, uint32_t length){
	uint32_t i = 0;
	// uint8_t length = sizeof(data)

	#if arch == frdm
	for(i = 0; i < length; i++){
		put_uart0(*data);
		data++;
	}
	#endif

	#if arch == host
	for(i = 0; i < length; i++){
		char dat = (char)*data;
		printf("%c", dat);
		data++;
	}
	#endif
}
