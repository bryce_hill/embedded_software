#include "peripherals.h"

uint8_t initialize_rgbled(void){
	#if arch == frdm
		SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;
		SIM_SCGC5 |= SIM_SCGC5_PORTD_MASK;
		PORTD_BASE_PTR->PCR[1] = PORTB_BASE_PTR->PCR[18] = PORTB_BASE_PTR->PCR[19] = PORT_PCR_MUX(1);
		GPIOB_PDDR  |= 0x000C0000; // Set pin B18 and B19 (Red and Green cathodes respectively) as outputs
		GPIOD_PDDR  |= 0x00000002; // Set pin D1 (Blue cathode) as output
		//  GPIOB_PSOR   = 0x000C0000; // Set (turn off) the red and green pins
		//  GPIOD_PSOR   = 0x00000001; // Set (turn off) the blue pin
		return(SUCCESS);
	#endif
}

uint8_t toggle_led (color_t RGB_LED){
	#if arch == frdm
		switch (RGB_LED) {
		case RED:
			GPIOB_PTOR = 0x00040000; // Toggle red
			break;
		case GREEN:
			GPIOB_PTOR = 0x00080000; // Toggle green
			break;
		case BLUE:
			GPIOD_PTOR = 0x00000001; // Toggle blue
			break;
		case ALL:
		    GPIOB_PTOR = 0x000C0000;
		    GPIOD_PTOR = 0x00000001;
		default:
	    	return(FAILURE);
		}
  		return(SUCCESS);
	#endif
}

uint8_t led_on (color_t RGB_LED){
	#if arch == frdm
		switch (RGB_LED) {
		case RED:
		  GPIOB_PCOR = 0x00040000; // Clear red
		  break;
		case GREEN:
		  GPIOB_PCOR = 0x00080000; // Clear green
		  break;
		case BLUE:
		  GPIOD_PCOR = 0x00000002; // Clear blue
		  break;
		case ALL:
		  GPIOB_PCOR = 0x000C0000;
		  GPIOD_PCOR = 0x00000001;
		default:
		  return(FAILURE);
  		}
  		return(SUCCESS);
	#endif
}

uint8_t led_off (color_t RGB_LED){
	#if arch == frdm
		switch (RGB_LED) {
		case RED:
		  GPIOB_PSOR = 0x00040000; // Set red
		  break;
		case GREEN:
		  GPIOB_PSOR = 0x00080000; // Set green
		  break;
		case BLUE:
		  GPIOD_PSOR = 0x00000002; // Set blue
		  break;
		case ALL:
		    GPIOB_PSOR = 0x000C0000;
		    GPIOD_PSOR = 0x00000001;
		default:
		  return(FAILURE);
		}
  		return(SUCCESS);
	#endif
}

void user_input_pwm_led(void){
    #if arch == frdm
        if(bufferStatus(RXbuffer).status!=BUFF_EMPTY){
			uint8_t message = 0;
			uint8_t * messagePtr = &message;
            RXbuffer = bufferPop(RXbuffer, messagePtr, 1);
            switch (*messagePtr) {
                #if PWM == ON
                    case 'r':   // red
                        TPM2_C0V = DUTY_MAX;
                        TPM2_C1V = DUTY_MIN;
                        TPM0_C1V = DUTY_MIN;
                        break;
                    case 'y':   // yellow
                        TPM0_C0V = DUTY_MED;
                        TPM2_C1V = DUTY_MED;
                        TPM0_C1V = DUTY_MIN;
                        break;
                    case 'g':   // green
                        TPM0_C0V = DUTY_MIN;
                        TPM2_C1V = DUTY_MAX;
                        TPM0_C1V = DUTY_MIN;
                        break;
                    case 'c':   // cyan
                        TPM0_C0V = DUTY_MIN;
                        TPM2_C1V = DUTY_MED;
                        TPM0_C1V = DUTY_MED;
                        break;
                    case 'b':   // blue
                        TPM0_C0V = DUTY_MIN;
                        TPM2_C1V = DUTY_MIN;
                        TPM0_C1V = DUTY_MAX;
                        break;
                    case 'p':   // purple
                        TPM0_C0V = DUTY_MED;
                        TPM2_C1V = DUTY_MIN;
                        TPM0_C1V = DUTY_MED;
                        break;
                    default:    // white
                        TPM0_C0V = DUTY_MED;
                        TPM2_C1V = DUTY_MED;
                        TPM0_C1V = DUTY_MED;
                        break;
                #endif
                #if PWM == OFF
                    case 'r':   // red
    					led_on(RED);
    					led_off(GREEN);
    					led_off(BLUE);
                        break;
                    case 'y':   // yellow
                        break;
                    case 'g':   // green
    					led_on(GREEN);
    					led_off(RED);
    					led_off(BLUE);
                        break;
                    case 'c':   // cyan
                        break;
                    case 'b':   // blue
    					led_on(BLUE);
    					led_off(GREEN);
    					led_off(RED);
                        break;
                    case 'p':   // purple
                        break;
                    default:    // white
                        break;
                #endif
            }
        }
    #endif
}
