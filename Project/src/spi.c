#include "spi.h"

void initialize_spi0(void){
	#if arch == frdm
		SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK; 	// Enable clock to port C
		SIM_SCGC4 |= SIM_SCGC4_SPI0_MASK;	// Enable clock to SIM_SCGC4_SPI0_MASK

		PORTC_PCR0 = PORT_PCR_MUX(0X1);		// Set PTC0 (CE - not actually part of SPI0 but needed for nRF24L01 module) to mux 1
		PORTC_PCR10 = PORT_PCR_MUX(0X1);	// Set PTC4 (PCS) to mux 2
		GPIOC_PDDR  |= 0x01; 		// Set pin C0 as output
		GPIOC_PDDR  |= CSN; 		// Set pin C4 as output

	 	PORTC_PCR5 = PORT_PCR_MUX(0X2);	// Set PTC5 (SCK) to mux 2
		PORTC_PCR6 = PORT_PCR_MUX(0X2);	// Set PTC6 (MOSI) to mux 2
		PORTC_PCR7 = PORT_PCR_MUX(0X2);	// Set PTC7 (MISO) to mux 2

		SPI0_C1 = SPI_C1_MSTR_MASK;
		SPI0_C1 &= ~SPI_C1_SSOE_MASK;		// Set SPI0 to master & SS pin to auto
		SPI0_C1 |= SPI_C1_SPIE_MASK;						// Enable SPI interrupts
		SPI0_C1 &= ~SPI_C1_CPHA_MASK;
		NVIC_EnableIRQ(SPI0_IRQn);
		SPI0_C2 &= ~SPI_C2_MODFEN_MASK;   					// Master SS pin acts as slave select output

		SPI0_BR = (SPI_BR_SPPR(0x02) | SPI_BR_SPR(0x08));  	// Set baud rate prescale divisor to 3 & set baud rate divisor to 64 for baud rate of 15625 hz

		SPI0_C1 |= SPI_C1_SPE_MASK;    		// Enable SPI0

		csn_hold = 0;
		chipSelect(SET);
	#endif
}

void write_spi0(uint8_t *spimsg){
	#if arch == frdm
	uint8_t i = 0;
		for(i = 0; i < sizeof(spimsg); i++){
			put_spi0(*spimsg);
			spimsg++;
		}
	#endif
}

void put_spi0(uint8_t byte){
	#if arch == frdm
		chipSelect(CLEAR);
		while(!(SPI_S_SPTEF_MASK & SPI0_S)){/* TODO asm commands don't worky*/}
		SPI0_D = byte;	// Write byte to SPI
		while(!(spi0_rxflag)){/* TODO asm commands don't worky*/}
		spi0_rxflag = 0;
		chipSelect(SET);
	#endif
}

void chipSelect(CSE_t action){
	#if arch == frdm
		if(action == CLEAR){
			GPIOC_PCOR = CSN; // Clear
		}
		else if(action == SET && csn_hold == 0){
			GPIOC_PSOR = CSN; // Set
		}
	#endif
}
