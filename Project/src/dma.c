#include "dma.h"

uint8_t dma_task_step = 0;

void dma_config_0(void){
  // Chapter 23
  //  Four channels (n = 0 through 3)
  //    SARn Source Address Register
  //    DARn Destination Address Register
  //    DSRn Status Regsister
  //    BCRn Byte Count Regsister
  //    DCRn Control Regsister
  //    ---
  //    DREQn Data Request (from peripeheral?)
  //    DACKn DMA Acknowledegement

  // OUTLINE:
    // 0) Enable clock to DMA & DMAMUX
  // 1) Set up DMA MUX
  // 2) Configure channel
  // 3) Initialize DMAn
    // a) Set Source (DMA_SARn)
    // b) Set Destination (DMA_DARn)
    // c) Set Length (DMA_DSR_BCRn)
  // 4) Start DMA: DCRn[START]=1
  // 5) Wait for BCRn to decrement to 0, which will set DSRn[DONE] and throw an interrupt

// 0)
    SIM_SCGC6 |= SIM_SCGC6_DMAMUX_MASK;
    SIM_SCGC7 |= SIM_SCGC7_DMA_MASK;

// 1)
  DMAMUX0_CHCFG0 &= ~DMAMUX_CHCFG_ENBL_MASK;   // 0) Turn off DMA
  // DMAMUX0_CHCFG |= DMAMUX_CHCFG_SOURCE(0) // Change the DMA MUX Source

// 2)
  DMA_DCR0 |= DMA_DCR_EINT_MASK; // Enable interrupt on COMPLETE
  DMA_DCR0 |= DMA_DCR_ERQ_MASK; //
  // Leave Peripheral requests off
  // Leave Cycle steal off
  // Leave auto-alignment off
  // Leave async DMA requests off
  // Leave Source increment
  DMA_DCR0 |= DMA_DCR_SSIZE(1); // Choose 8-bit source size
  DMA_DCR0 |= DMA_DCR_DINC_MASK; // Turn on "incrementation of destination after COMPLETE"
  DMA_DCR0 |= DMA_DCR_SINC_MASK; // Turn on "incrementation of source after COMPLETE"
  DMA_DCR0 |= DMA_DCR_DSIZE(1); // Choose 8-bit data disze of the destination
  DMA_DCR0 |= DMA_DCR_SMOD(7); // Change Source address modulo (circ buff size) to 1KB
  DMA_DCR0 |= DMA_DCR_DMOD(7); // Change destination address modulo (circ buff size) to 1KB
  // Leave disable request register off
  // Leave channel-linking off

  DMAMUX0_CHCFG0 |= DMAMUX_CHCFG_ENBL_MASK;     // 0) Turn on DMA
  NVIC_EnableIRQ(DMA0_IRQn);                    // Enable interrupt

  return;
}

void dma_start(volatile uint8_t * src,volatile uint8_t * dest, uint16_t length){
  LOG0("**** Starting DMA **** \r\n");
  dma_task_step = 1;
  dma_timer = restart_timer(dma_timer);
  dma_memmove(src,dest,length);
  // get_systicks();
  // diff_systicks(dma_timer.start_time, dma_timer.stop_time);
  return;
}

void dma_task(void){
    float conversion = .00002083; // cycles 2 s

    // float t;
    int8_t m[30];
    int8_t *mptr = &m[0];
	int8_t numRes = 0;

    if(dma_task_step == 1 && dma_timer.timer_active == 0){
        LOG0("My DMA Profiler Time: ");
        // Log results
        uint64_t t = diff_systicks(dma_timer.start_time, dma_timer.stop_time);

        uint32_t tt = (uint32_t)t;
		numRes = my_itoa(mptr, tt);

		LOG8(mptr, numRes);
        LOG0(" [cycles] | ");
        LOG0(" // ");
        numRes = my_itoa(mptr, dma_timer.start_time);
        LOG8(mptr, numRes);
        LOG0(" // ");
        numRes = my_itoa(mptr, dma_timer.stop_time);
        LOG8(mptr, numRes);
        LOG0(" // ");

        float ts = conversion*t;
        int32_t res[10] = {0};
        int32_t *resPtr = &res[0];
        resPtr = my_ftoa(ts, resPtr, 9);
        LOG32(resPtr, 9);
        LOG0(" [seconds]\n\r");
        dma_task_step = 0;
  }
  return;
}

void dma_memmove(volatile uint8_t *source, volatile uint8_t *destination, uint8_t size){
    count = size;
// 3)
  DMA_DAR0 = (int)destination;
  DMA_SAR0 = (int)source;
  DMA_DSR_BCR0 = size;
  // DMA_DSR_BCR0 |= DMA_DSR_BCR_BCR(count);		// Set byte count register
// 4)
  DMA_DCR0 |= DMA_DCR_START_MASK; // Start transfer
  return;
}
