#include "circBuffer.h"

CircBuf_t createBuffer(CircBuf_t buff, uint16_t size){
    buff.bufferSize=size;
    buff.buffMin = malloc(buff.bufferSize);
    buff.buffHead = buff.buffMin;
    buff.buffTail = buff.buffMin;
    buff.buffMin = buff.buffMin;
    buff.buffMax = buff.buffMin + buff.bufferSize;
    buff.wrap = 0;
    return(buff);
}

uint8_t destroyBuffer(CircBuf_t buff){
    free(buff.buffMin);
    return(SUCCESS);
}

CircBuf_t bufferStatus(CircBuf_t buff){
  if(buff.buffHead==buff.buffTail){
    if(buff.wrap==0){
      buff.status = BUFF_EMPTY;
	//   uint8_t msg[] = "*Buffer EMPTY*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
    else{
      buff.status=BUFF_FULL;
	//   uint8_t msg[] = "*Buffer FULL - Has WRAPPED*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
  }
  // else if(buff.buffHead == buff.buffMax){
  //  buff.status=BUFF_OVERFLOW;
  //     uint8_t msg[] = "*Buffer OVERFLOW*\n\r";
  //  LOG8(msg, sizeof(msg));
  // }
  else if(buff.buffHead>buff.buffTail){
    if(buff.wrap==0){
      buff.status=BUFF_NORMAL;
	//   uint8_t msg[] = "*Buffer NORMAL*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
    else{
      buff.status=BUFF_OVERFLOW;
	//   uint8_t msg[] = "*Buffer OVERFLOW*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
  }
  else if(buff.buffHead<buff.buffTail){
    if(buff.wrap==0){
      buff.status=BUFF_OVERFLOW;
	//   uint8_t msg[] = "*Buffer OVERFLOWED*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
    else{
      buff.status=BUFF_NORMAL;
	//   uint8_t msg[] = "*Buffer NORMAL*\n\r";
	//   LOG8(msg, sizeof(msg));
    }
  }
  return(buff);
}


CircBuf_t bufferPush(CircBuf_t buff, uint8_t *src, uint8_t length){
  buff = bufferStatus(buff);
  //if(buff.status==BUFF_FULL || buff.status==BUFF_OVERFLOW){
  if(buff.status==BUFF_OVERFLOW){
    bufferLog(buff);
    return(buff);
  }
  uint8_t i=0;
  for(i;i < length;i++){
    buff = bufferStatus(buff);
    switch (buff.status) {
      case BUFF_FULL:
	      bufferLog(buff);
	      return(buff);
	      break;
      case BUFF_OVERFLOW:
		  bufferLog(buff);
		  // createBuffer(buff, buff.bufferSize);
		  return(buff);
      break;
    }
	*buff.buffHead=*src;

    if(buff.buffHead==buff.buffMax){
      buff.buffHead=buff.buffMin;
      buff.wrap=1;
      buff = bufferStatus(buff);
	//   printf("Check 1\n");
    //   bufferLog(buff);
    }
	else{
		buff.buffHead++;
	}
    src++;
	if(buff.buffCounter != buff.bufferSize){
		buff.buffCounter++;
	}
  }
  return(buff);
}


CircBuf_t bufferPop(CircBuf_t buff, uint8_t *dest, uint8_t length){
  buff = bufferStatus(buff);
  if(buff.status==BUFF_EMPTY){
      bufferLog(buff);
      return(buff);
  }
  // printf("Check 1: length = %d\n", length);
  uint8_t i=0;
  for(i = 0;i < length;i++){
    // if(i =! 0){dest++;};
    buff = bufferStatus(buff);
    switch (buff.status){
      case BUFF_EMPTY:
      bufferLog(buff);
        return(buff);
      break;
      case BUFF_OVERFLOW:
        bufferLog(buff);
        // createBuffer(buff, buff.bufferSize); // To destroy buffer
        return(buff);
      break;
    }
    // printf("Check 2: i = %d\n", i);
    // printf("Check 3\n");
    // printf("Check 4\n");
	// printf("buffTail = %p | %d", buff.buffTail, *buff.buffTail);
	// printf("dest %p\n", dest);
	// printf("dest val %d\n", *dest);
	// printf("Check 5\n");

	*dest=*buff.buffTail;

	if(buff.buffTail==buff.buffMax){
		buff.buffTail=buff.buffMin;
		buff.wrap=0;
	}
	else{
		buff.buffTail++;
	}

	buff.buffCounter--;

	if(i < length-1){
		// printf("We shouldn't be getting here");
		dest++;
	}
	// printf("buff counter = %d\n", buff.buffCounter);
  }
  return(buff);
}

void bufferPrint(CircBuf_t buff){
    #if arch == frdm
		//TODO - itoa doesn't handle 0 is cases e.g. 30 ->3, also update to match host's method
		// uint8_t size = abs(buff.buffHead - buff.buffTail);
		// printf("size %d", size);
		// uint8_t* sizeptr = &size;

		uint8_t msg[] = "\n-- Printing Buffer Data -- \n\rBuffer size: ";
		LOG8(msg, sizeof(msg));

		// uint8_t sizei = size;
		// my_itoa(sizeptr, (int32_t)size);
		// LOG8(sizeptr, sizeof(size));

		// uint8_t msg3[] = "\n";
		// LOG8(msg3, sizeof(msg3));
        int8_t size = buff.buffCounter;

		int8_t i = 0;
		// uint8_t dum1[] = "000";
		uint8_t* dummy = buff.buffTail;
		// uint8_t res = my_itoa(dum1, (int32_t)sizei);
		for(i; i < size; i++){
			// uint8_t dum[] = "000";
			// uint8_t res = my_itoa(dum, (int32_t)*dummy);
			// LOG8(dum, res);
			// uint8_t msg[] = "\n";
            if(dummy == buff.buffMax){
                dummy = buff.buffMin;
            }
            uint8_t data[1];
            data[0] = *dummy;
			LOG8(data, sizeof(data));
			dummy++;
		}
		uint8_t msg2[] = "\n\r~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
		LOG8(msg2, sizeof(msg2));
    #endif

    #if arch == host
		int8_t size = buff.buffCounter;

    	printf("-- Printing Buffer Data -- \n");
    	printf("Buffer size: %d\n", size);
    	int8_t i = 0;
		uint8_t* dummy = buff.buffTail;
		for(i; i < size; i++){
			if(dummy == buff.buffMax){
				dummy = buff.buffMin;
				printf("wrapp here:\n");
			}
			printf("Data: %d = %d\n", i, *dummy);
			dummy++;
		}
    	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    #endif

}

void bufferLog(CircBuf_t buff){
    #if DEBUG == 1
        if(buff.status == BUFF_FULL){
            uint8_t l[] = "BUFFER FULL\n\r";
            LOG8(l, sizeof(l));
        }
        else if(buff.status == BUFF_WRAP){
            uint8_t l[] = "BUFFER WRAP & OVERFLOW\n\r";
            LOG8(l, sizeof(l));
        }
        else if(buff.status == BUFF_OVERFLOW){
            uint8_t l[] = "BUFFER OVERFLOW\n\r";
            LOG8(l, sizeof(l));
        }
        else if(buff.status == BUFF_EMPTY){
            uint8_t l[] = "BUFFER EMPTY\n\r";
            LOG8(l, sizeof(l));
        }
        else {
            uint8_t l[] = "BUFFER NORMAL\n\r";
            LOG8(l, sizeof(l));
        }
    #endif
}
