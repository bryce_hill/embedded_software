
#include "timer.h"

// struct{
// 	uint64_t wait_timer_1;
// 	uint8_t wait_rollover_1;
// 	uint64_t wait_timer_2;
// 	uint8_t wait_rollover_2;
// 	uint64_t wait_timer_3;
// 	uint8_t wait_rollover_3;
// } wait_timers_list;

uint8_t timer_init(void){
    //TODO: Check ordering (disabling channels before config, "clock domains"?)
    SIM_SCGC6 |= SIM_SCGC6_TPM0_MASK;       // Enable clock to gating to TPM module
	SIM_SCGC6 |= SIM_SCGC6_TPM1_MASK;       // Enable TPM1 clock
    SIM_SCGC6 |= SIM_SCGC6_TPM2_MASK;       // Enable TPM2 clock

    SIM_SOPT2 |= SIM_SOPT2_TPMSRC(1);       // Enable MCGFLLCLK to TPM module (all)

    // Status and Control (Interrupt Enable)
	// - CMOD(1) -> LPTPM counter increments on every LPTPM counter clock
	// - PS(7) -> 128 prescale factor (i.e. divide by 128)
	TPM0_BASE_PTR->SC = TPM_SC_CMOD(1) | TPM_SC_PS(7);
	TPM0_BASE_PTR->MOD = 16383;
    TPM2_BASE_PTR->SC = TPM_SC_CMOD(1) | TPM_SC_PS(7);
    TPM2_BASE_PTR->MOD = 16383;
	// - PS(0) -> 1 prescale factor (i.e. divide by 1)
    TPM1_BASE_PTR->SC = TPM_SC_CMOD(1) | TPM_SC_PS(0);
    TPM1_BASE_PTR->MOD = 0xFFFF;

	// Enable interrupts routines defined in MKL25Z4.h (i.e. module_IRQHandler() fcns)
	// - TPM0_C0SC &= TPM_CnSC_CHIE_MASK; 	// Enable channel 0 interrupts (Mo had this in originally but it doesnt seem to be needed? -Bryce)
	NVIC_EnableIRQ(TPM0_IRQn);
    NVIC_EnableIRQ(TPM1_IRQn);
	NVIC_EnableIRQ(TPM2_IRQn);

	// Enable interrupts on timer overflow (TOF)
    TPM1_BASE_PTR->SC |= TPM_SC_TOIE_MASK;


    #if PWM == ON   // (link LEDs to PWMs)
		// Enable clock gate control to ports B & D
		SIM_BASE_PTR->SCGC5 |= SIM_SCGC5_PORTB_MASK;
		SIM_BASE_PTR->SCGC5 |= SIM_SCGC5_PORTD_MASK;

		// Channel 0 Status and Control (Channel-Interrupt Mode)
		/* pg567: "PWM Mode is selected when CPWMS=0 an dMSnB:MSnA=1:0.
		"If ELSnB:ELSnA=0:0 when the counter reaches the value in the CnV
		register...[an interrupt occurs but the output is not changed]. If
		ELSnB:ELSnA=1:0,...[the output _is_ changed]"  TPM0_C0SC.CHF = 1;
		Clear the Channel Flag (yes, 1 clears it) */
        PORTB_BASE_PTR->PCR[18] = PORT_PCR_MUX(3);
        PORTB_BASE_PTR->PCR[19] = PORT_PCR_MUX(3);
        PORTD_BASE_PTR->PCR[1] = PORT_PCR_MUX(4);

        PTB_BASE_PTR->PDDR = 1 << 18;
        PTB_BASE_PTR->PDDR = 1 << 19;
        PTD_BASE_PTR->PDDR = 1;

		// Set all pwm timers to pcounting mode (see pg568)
		TPM0_SC |= TPM_SC_CPWMS_MASK;
		TPM1_SC |= TPM_SC_CPWMS_MASK;
		TPM2_SC |= TPM_SC_CPWMS_MASK;

		// Now remove the code responsible for enabling the interrupts and configure
		// the channel 0 of TPM2 to run in the PWM mode and generate short impulses
		// (1/8 of the timer period):
		// * See pg 555, table 31-34

		// --Pulse Width and Duty Cycle --
		/* pg. 567: "The EPWM period is determined by (MOD+0x0001) and the puse width
		(duty cycle) is determined by CnV"
		pg. 568: "If CnV=0x000 them the channel n output is 0% duty cycle. If
		CnV>MOD, then...[it's] 100% duty cycle" */
	    TPM2_BASE_PTR->CONTROLS[0].CnSC = TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK;
	    TPM2_BASE_PTR->CONTROLS[0].CnV = TPM2_BASE_PTR->MOD / 8;

	    TPM2_BASE_PTR->CONTROLS[1].CnSC = TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK;
	    TPM2_BASE_PTR->CONTROLS[1].CnV = TPM2_BASE_PTR->MOD / 8;

	    TPM0_BASE_PTR->CONTROLS[1].CnSC = TPM_CnSC_MSB_MASK | TPM_CnSC_ELSA_MASK;
	    TPM0_BASE_PTR->CONTROLS[1].CnV = TPM2_BASE_PTR->MOD / 8;
	#endif
  	// my_isr_flags = 0;		// Initialize isr flags TODO Currently unused, was part of Mo's LED scheme
}

my_timer_t create_timer(my_timer_t timer){
	#if arch == frdm
		timer.timer_active = 0;
		timer.stop_time = 0;
		timer.start_time = 0;
		timer.overflow_count = 0;
		// SIM_SCGC6 |= SIM_SCGC6_TPM0_MASK;	// Already done in timer_init
		// SIM_SOPT2 |= SIM_SOPT2_TPMSRC(01); 	// Already done in timer_init
		return(timer);
	#endif
}

my_timer_t restart_timer(my_timer_t timer){
  timer.stop_time = 0; 		// Clear the stop time
  timer.overflow_count = 0; // Clear the overflow counter
  timer.timer_active = 1; 	// Turn on the timer
  timer.start_time = get_systicks(); // Set to the current my_systicks time
  return(timer);
}

my_timer_t resume_timer(my_timer_t timer){
  timer.timer_active = 1; // Turn on the timer
  return(timer);
}

my_timer_t stop_timer(my_timer_t timer){
  timer.stop_time = get_systicks(); // Set to current my_systicks time
  timer.timer_active = 0; // Turn off the timer
  return(timer);
}

void delay_timer(uint16_t delay_time_us){
	uint64_t current_time = get_systicks();
	uint64_t delay_time_systicks = us_to_ticks(delay_time_us);
	uint64_t alarm_time = delay_time_systicks + current_time;
	uint8_t alarm_rollover = 0;
	if(alarm_time < current_time){alarm_rollover = 1;}
	if(wait_timers_list.wait_timer_1 == 0){wait_timers_list.wait_timer_1 = alarm_time; wait_timers_list.wait_rollover_1 = alarm_rollover;}
	else if(wait_timers_list.wait_timer_2 == 0){wait_timers_list.wait_timer_2 = alarm_time; wait_timers_list.wait_rollover_2 = alarm_rollover;}
	else if(wait_timers_list.wait_timer_3 == 0){wait_timers_list.wait_timer_3 = alarm_time; wait_timers_list.wait_rollover_3 = alarm_rollover;}
	else{LOG0("NO MORE TIMER SLOTS AVAILABLE");}
	return;
}

void my_delay(uint16_t delay_us){
    uint64_t start_time = get_systicks();
    uint64_t delay_time = us_to_ticks(delay_us);
    uint8_t rollover = 0;
    uint64_t remain = delay_time%0xffff;
    if(delay_time > 0xffff){
        rollover = delay_time/0xffff;
    }

    uint8_t flag = 0;
    uint64_t now_time = get_systicks();
    // if(rollover == 0){
    while(flag != 1){
        now_time = get_systicks();
        if((now_time - start_time) > delay_time){flag = 1;}
        else if(now_time < start_time){
            if(0xffff - start_time + now_time > delay_time){flag = 1;}
        }
    }
    // }
    // else if(rollover != 0){
    //     while(rollover =! tpm1Flag){
    //         // do nothing
    //     }
    //     tpm1Flag == 0;
    //     while(flag != 1){
    //         now_time = get_systicks();
    //         if((now_time - start_time) > delay_time){flag = 1;}
    //         else if(now_time < start_time){
    //             if(0xffff - start_time + now_time > delay_time){flag = 1;}
    //         }
    //     }
    // }
}

uint8_t delay_task(void){
	static uint8_t delay_status = 0;
	uint64_t current_ticks = get_systicks();
	static uint64_t previous_systick = 0;
	if(current_ticks < previous_systick){
		// systicks has rolled over
		wait_timers_list.wait_rollover_1 = 0;
		wait_timers_list.wait_rollover_2 = 0;
		wait_timers_list.wait_rollover_3 = 0;
	}
	if((wait_timers_list.wait_timer_1 != 0) && (wait_timers_list.wait_rollover_1 == 0) && (current_ticks >= wait_timers_list.wait_timer_1)){
		delay_status |= 0x01;
		wait_timers_list.wait_timer_1 = 0;
	}
	if((wait_timers_list.wait_timer_2 != 0) && (wait_timers_list.wait_rollover_2 == 0) && (current_ticks >= wait_timers_list.wait_timer_2)){
		delay_status |= 0x02;
		wait_timers_list.wait_timer_2 = 0;
	}
	if((wait_timers_list.wait_timer_3 != 0) && (wait_timers_list.wait_rollover_3 == 0) && (current_ticks >= wait_timers_list.wait_timer_3)){
		delay_status |= 0x03;
		wait_timers_list.wait_timer_3 = 0;
	}
	previous_systick = get_systicks();
	return(delay_status); // TODO: Figure out what to do with these "alarm" flags outside of timer.c
}

uint64_t get_systicks(void){
	#if arch == frdm
		uint64_t subticks = 0;
		subticks = my_systicks + (TPM1_BASE_PTR->CNT);
		return(subticks);
	#endif
}

uint64_t us_to_ticks(uint16_t us){
	uint64_t ticks = us * (uint64_t)(SystemCoreClock*.000001); // TODO: Figure out conversion factor
	return(ticks);
}

uint64_t diff_systicks(uint64_t start_ticks, uint64_t end_ticks){
  uint64_t ticks = 0;
  if(start_ticks > end_ticks){
    ticks = (SYSTICKS_OV - start_ticks) + end_ticks;
  }
  else{
    ticks = end_ticks - start_ticks;
  }
  return(ticks);
}

float sec_st(uint64_t my_systicks){
  float tick2ms_conversion_factor = 1;
  return((float)my_systicks / tick2ms_conversion_factor);
}
