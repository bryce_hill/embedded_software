#include "macro.h"
#include "init.h"
#include "peripherals.h"
#include "project_2.h"
#include "project_3.h"
#include "macro.h"
#include "spi.h"
#include "memory.h"
#include "timer.h"
#include "dma.h"
#include "nRF24L01.h"

#if arch == frdm
	#include "MKL25Z4.h"
#endif

int main (void) {
	initialize_ARCH();

    #if arch == frdm
        //Attach a TX and RX ring buffer to the UART0
        TXbuffer=createBuffer(TXbuffer,128);
        TXbuffer.buffCounter = 0;
        RXbuffer=createBuffer(RXbuffer,128);
        RXbuffer.buffCounter = 0;
    #endif
	// my_project_1_report();
	// my_project_2_report();
	// my_project_3_report();

	while(1){
		#if arch == frdm
			cmd_task();			// Handle user input via UART
			nRF24L01_task(); 	// nRF24L01 module related tasks
			alarm_flag = delay_task();		// Handle requested alarms
		#endif
     }
	 return(0);
}
