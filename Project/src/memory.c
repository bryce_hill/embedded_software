#include "memory.h"

my_timer_t my_memmove_timer;

uint8_t my_memmove(uint8_t * src, uint8_t * dest, uint32_t length){
    uint8_t overlapFlag=1;
    if(length==0){return(FAILURE);}
    if(dest<=src+length && dest>src){
      overlapFlag=-1;
      dest=dest+length;
      src=src+length;
    } // Prevent copying to memory that we shouldn't be writing to

    uint8_t i = 0;
    for (i=0;i<length;i=i+overlapFlag){
        *dest = *src;
        dest=dest+overlapFlag;
        src=src+overlapFlag;
    }
    return(SUCCESS);
}

uint8_t my_memzero(uint8_t * src, uint32_t length){
    if(length==0){return(FAILURE);}
    if(src==0){return(FAILURE);} // Prevent clearing memory that we shouldn't be clearing to
    uint8_t i = 0;
    for (i=0;i<length;i++){
        *src=0;
        src++;
    }
    return(SUCCESS);
}

uint8_t my_reverse(uint8_t * src, uint32_t length){
    if(length==0){return(FAILURE);}
    // if(src+length>=MEMORY_BOUND_UPPER){return(FAILURE);};
    uint8_t * end = src+length-1;
    uint8_t temp=0;
    while(src<=end){
        temp=*src;
        *src=*end;
        *end=temp;
        src++;
        end--;
    }
    return(SUCCESS);
}

void mymemmove_profiler(uint8_t * src, uint8_t * dest, uint32_t length){
    my_memmove_timer = restart_timer(my_memmove_timer);
    my_memmove(src, dest, length);
    my_memmove_timer = stop_timer(my_memmove_timer);

    LOG0("My Memmove Profiler Time: ");
    float conversion = .00002083;   // cycles 2 s
    int8_t numRes = 0;
    int8_t m[30];
    int8_t *mptr = &m[0];

    uint64_t t = diff_systicks(my_memmove_timer.start_time,my_memmove_timer.stop_time);

	uint32_t tt = (uint32_t)t;
	numRes = my_itoa(mptr, tt);

    LOG8(mptr, numRes);
    LOG0(" // ");
    numRes = my_itoa(mptr, my_memmove_timer.start_time);
    LOG8(mptr, numRes);
    LOG0(" // ");
    numRes = my_itoa(mptr, my_memmove_timer.stop_time);
    LOG8(mptr, numRes);
    LOG0(" // ");

    float ts = conversion*t;
    int32_t res[10] = {0};
    int32_t *resPtr = &res[0];
    resPtr = my_ftoa(ts, resPtr, 9);

    LOG0(" [cycles] | ");
    LOG32(resPtr, 9);
    LOG0(" [seconds]\n\r");
    LOG0(" // ");
    numRes = my_itoa(mptr, (int32_t)(tt*conversion*10000));
    LOG8(mptr, numRes);
    LOG0("\n\r");
    // LOG32(resPtr, 9);
    return;
}
