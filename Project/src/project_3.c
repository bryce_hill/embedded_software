#include "project_3.h"

void my_project_3_report(){
	// This was test code for CMD interface in project 3
	#if arch == host
		// Test cmd parsing in message files
		CircBuf_t RXbuffer;
		RXbuffer.buffCounter = 0;
		RXbuffer = createBuffer(RXbuffer,128);

		int8_t src[5] = {FRDM_LED_CONTROL, 3, RED, ON, 7};
		int8_t * srcPtr;
		srcPtr = &src[0];
		RXbuffer = bufferPush(RXbuffer, srcPtr, 5);
		// bufferPrint(RXbuffer);

		Instantiate_Struct(RXbuffer);
	#endif

	// Perform DMA vs. MemMove profiling test
	#if arch == frdm
		uint8_t test[] = "abcdefghijklmnopqrstuvwx\n\r";
		uint8_t *testPtr = &test[0];
		uint8_t destTest[] = "000000000000000000000";
		uint8_t *destTestPtr = &destTest[0];

		mymemmove_profiler(testPtr, destTestPtr, sizeof(test));
		LOG0("Printing data found at destination memory location:\n\r");
		LOG8(destTest, sizeof(destTest));
		LOG0("\n\r");


		volatile uint8_t testdma[] = "abcdefghijklmnopqrstuvwx\n\r";
		volatile uint8_t *testPtrDma = &testdma[0];
		volatile uint8_t destTestDma[] = "000000000000000000000";
		volatile uint8_t *destTestPtrDma = &destTestDma[0];
		dma_start(testPtrDma, destTestPtrDma, sizeof(testdma));

        while(1){
            dma_task();
        }
		LOG0("Printing data found at destination memory location:\n\r");
		LOG8(destTestDma, sizeof(destTestDma));
		LOG0("\n\r");
	#endif
}
