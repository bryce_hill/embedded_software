#include "data.h"

uint8_t my_itoa(uint8_t * str, int32_t data){
  uint8_t isNegative=0;
  uint8_t numberOresults = 0;
  if(data < 0){
    isNegative=1;
    data=data*(-1);
    *str = 0x2D;
    str++;
  }
  uint32_t place;
  uint8_t digit;
  uint8_t foundDigit_f=0;
  for(place=1000000000; place>=1; place=place/10){
    digit=data/place;
    data=data-(digit*place);
    if(digit!=0 || foundDigit_f==1){
      foundDigit_f = 1;
      *str = digit+0x30;
    //   printf("data %d\n", *str);
      str++;
	  numberOresults++;
    }
  }
  return(numberOresults);
}

int32_t my_atoi(uint8_t * str, uint32_t length, uint32_t base){
  uint8_t isNegative = 0;
  uint32_t operand = 0;
  uint32_t answer = 0;

  if(*str==0x2D){
    isNegative = 1;
    str++;
  }
  uint8_t i;
  for(i=0;i<length;i++){
    if(*str>=0x40){
      operand=*str-0x40;
    }
    else{
      operand=*str-0x30;
    }
    operand=operand*(base^(length-i));
    answer=answer+operand;
    str++;
  }
  if(isNegative==1){
    answer=answer*(-1);
  }
  return(answer);
}

void my_dump_memory(uint8_t * start, uint32_t length){
  uint8_t i;
  for(i=0;i<length;i++){
    printf("%x", *start);
    start++;
  }
  printf("\n");
  return;
}

uint32_t my_big_to_little(uint32_t big){
  uint32_t little;
  uint8_t i;
  little = ((0x000000FF & big)<<24)|
           ((0x0000FF00 & big)<<8)|
           ((0x00FF0000 & big)>>8)|
           ((0xFF000000 & big)>>24);
  /*
  for(i=0;i<32;i=i+8){
    if(i<16){
      little=little|((big&(0x000000FF<<i))<<(24-(i*2)));
    }
    else{
      little=little|((big&(0x000000FF<<i))>>((i*2)-24));
    }
}
  */
  return(little);
}

uint32_t my_little_to_big(uint32_t little){
  uint32_t big;
  uint8_t i;
  big = ((0x000000FF & little)<<24)|
        ((0x0000FF00 & little)<<8)|
        ((0x00FF0000 & little)>>8)|
        ((0xFF000000 & little)>>24);
  return(big);
}

typedef union{
	float f;
	struct{
		uint32_t mantissa : 23;
    	uint32_t exponent : 8;
    	uint32_t sign : 1;
	} segments;
} floatUnion;

int32_t* my_ftoa(float data, int32_t* result, uint8_t precision){
	// ----- Variable Declaration ------------------------------
	// Union Variables
	floatUnion dat;
	dat.f = data;
	printf("Data in union : %f\n", dat.f);

	// Sign Variables
	int8_t sign = dat.segments.sign;

	// Exponent Variables
	uint32_t exponent = dat.segments.exponent - 127;

	// Integer Value Variables
	uint32_t integral = 0;
	int8_t numberOintegrals = 0;
	uint8_t integralArray[10] = {0};
	uint8_t* integralArrayPtr = integralArray;

	// Mantissa (fraction) Value Variables
	float fraction_f = 0;
	uint32_t fraction_i = 0;

	// General Variables
	int8_t index = 0;
	uint32_t asciiOffset = 48;
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ----- Add sign value to string if sign is negative -----
	if (sign = 1){
		// Value is negative
		result[0] = 0x2D;
		index++;
	}// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ----- Add integral part to string ----------------------
	integral = abs((uint32_t)data);
	numberOintegrals = my_itoa(integralArrayPtr, (uint32_t)integral);
	uint8_t k = 0;
	for(k; k < numberOintegrals; k++){
		result[index] = *integralArrayPtr;
		integralArrayPtr++;
		index++;
	}// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	// ----- Add decimal "." between integral and fraction ----
	result[index] = 0x2E;
	index++;
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// ----- Add Fraction -------------------------------------
	// Get mantissa and shift it by # of exponent
	if (exponent > 0){
		fraction_i = dat.segments.mantissa >> (23 - exponent - precision);
	}
	else if (exponent < 0){
		fraction_i = dat.segments.mantissa << abs(exponent);
	}
	else{
		fraction_i = dat.segments.mantissa;
	}

	// Get fraction value out of mantissa
	fraction_f = ((float)fraction_i*pow(2, -precision))*pow(10, precision);
	// Get rid of integral part of mantissa
	fraction_f = fraction_f - (int32_t)(fraction_f/(pow(10, precision)))*pow(10, precision);

	int32_t i = precision - 1;
	float dummy = fraction_f;
	for(i=precision -1; i>0; i--){
		result[index] = (int32_t)((dummy)/pow(10,i)) + asciiOffset;
		// printf("Frac loop: %d | Data: %f | Power: %d | Result: %d\n", index, dummy,(uint32_t)pow(10,i),result[index]);
		dummy  = dummy - (int32_t)((dummy)/pow(10,i))*pow(10,i);
		index++;
	}
	// Get least significant piece of decimal and add to string
	result[index] = ((uint32_t)fraction_f) % 10 +asciiOffset;
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	return(result);
}
