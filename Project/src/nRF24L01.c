#include "nRF24L01.h"

void write_registers(uint8_t *data, uint8_t reg, uint8_t size){
	uint8_t spimsg = W_REGISTER | reg;
	csn_hold = 1;
	put_spi0(spimsg);
	uint8_t i = 0;
	for(i = 0; i < size; i++){
		if(i == (size - 1)){
			csn_hold = 0;
		}
		put_spi0(data[i]);
	}
}

void write_payload(uint8_t *data, uint8_t size){
	csn_hold = 1;
	put_spi0(W_TX_PAYLOAD);
	uint8_t i = 0;
	for(i = 0; i < size; i++){
		if(i == (size - 1)){
			csn_hold = 0;
		}
		put_spi0(data[i]);
	}
	my_delay(8000);
	chipEnable(SET);
	my_delay(50);
	chipEnable(CLEAR);
	my_delay(200);
}

void write_command(uint8_t command){
	// uint8_t spimsg = W_TX_PAYLOAD;
	// csn_hold = 1;
	put_spi0(command);
}

void write_register(uint8_t reg, uint8_t val, CSE_t action){
	uint8_t spimsg;
	if(action == SET){
		spimsg = W_REGISTER | reg;
	}
	else if(action == CLEAR){
		spimsg = W_REGISTER ^ reg;
	}
	csn_hold = 1;
	put_spi0(spimsg);
	csn_hold = 0;
	put_spi0(val);
}

void read_register(uint8_t reg){
	uint8_t	spimsg = R_REGISTER | reg;
	csn_hold = 1;
	put_spi0(spimsg);
	csn_hold = 0;
	put_spi0(NOP);
	// TODO Want to automatically get result here, maybe set flag saying we want to look at what is coming in over SPI
}

void read_registers(uint8_t reg, uint8_t size){
	uint8_t spimsg = R_REGISTER | reg;
	uint8_t i = 0;
	csn_hold = 1;
	put_spi0(spimsg);
	for(i = 0; i < size; i++){
		if(i == (size - 1)){
			csn_hold = 0;
		}
		put_spi0(NOP);
	}
	// TODO Want to automatically get result here, maybe set flag saying we want to look at what is coming in over SPI
}

void chipEnable(CSE_t action){
	#if arch == frdm
		if(action == CLEAR){
			GPIOC_PCOR = 0x00000001; // Clear
		}
		else if(action == SET){
			GPIOC_PSOR = 0x00000001; // Set
		}
	#endif
}

void xmit_mode_select(XMIT_t mode){
	// CE - Set chip enable to enter RX/TX mode respectively
	if(mode == RXMODE){
		// Set PRIM_RX
		write_register(CONFIG, PRIM_RX |  PWR_UP | CRCO | EN_CRC, SET);
		chipEnable(SET);
	}
	else if(mode == TXMODE){
		// Clear PRIM_RX
		write_register(CONFIG,  PWR_UP | CRCO | EN_CRC, SET);
	}
	delay_timer(500);	// Delay for 200 us (only needs to be 150 us)
}

void nRF24L01_task(void){
	// if ((alarm_flag > 0) && (init_RF_flag < 4)){
	if ((alarm_flag > 0) && (init_RF_flag < 5)){
		init_RF_flag++;
	}
	#if NRFMODE == RECIEVER
		else if((init_RF_flag > 4) && (tpm1Flag > 15)){
			read_register(STATUS);
			tpm1Flag = 0;
		}
	#endif
	#if NRFMODE == TRANSMITTER
		else if((init_RF_flag > 4) && (tpm1Flag > 15)){
			uint8_t data[] = "54321";
			write_register(STATUS, MASK_MAX_RT, SET);
			write_command(FLUSH_TX);
			write_payload(data, 5);
			xmit_mode_select(TXMODE);
			tpm1Flag = 0;
		}
	#endif
	initialize_nRF24L01();
}

void initialize_nRF24L01(void){
	if(init_RF_flag == 0){
		// CONFIG
		// - Set PWR_UP = 1 -> turn on
		// - Wait 1.5 ms
		write_register(CONFIG, PWR_UP, SET);
		delay_timer(4300);	// Delay for 2000 us (only needes to be 1.5 ms)
		init_RF_flag++;
	}
	else if(init_RF_flag == 2){
		// EN_AA
		// - Enable auto acknowledegement for all pipes running enhanced shockburst (why 0x0?)
		write_register(EN_AA, 0x00, SET);

		// EN_RXADDR
		// - Enable data pipes 0 and 1 (following example)
		write_register(EN_RXADDR, 0x00, SET);

		// SETUP_AW
		// - Set reciever address to 5 bytes
		write_register(SETUP_AW, 0x3, SET);

		// RX_CH
		// - Set channel = 76 because example says this is stable
		write_register(RF_CH, 0x4c, SET);

		// RX_SETUP
		// - Set data rate TO 1 MBPS
		write_register(RF_SETUP, oneMBPS<<3 | 0x07, SET);

		// RF_ADDR_Px
		// - Set location of packet storage.. need?
		uint8_t data[] = {0xf0, 0xf0, 0xf0, 0xf0, 0xd2};
		uint8_t data1[] = {0xf0, 0xf0, 0xf0, 0xf0, 0xe1};
		write_registers(data, RX_ADDR_P0, 5);
		write_registers(data1, RX_ADDR_P1, 5);

		// TX_ADDR
		// - Set location of packet storage.. need?
		write_registers(data, TX_ADDR, 5);

		// RX_PW_Px
		// - Set payload widths for enabled pipes (0 & 1) = 32 bytes
		write_register(RX_PW_P0, 0x05, SET);
		write_register(RX_PW_P1, 0x05, SET);

		// CONFIG - PRIM_RX = 1 (arch = frdm -> reciever) else leave clear as transmitter
		// - Set CRCO = 1 -> 2 byte crc encoding scheme
		// - Set EN_CRC = 1 -> enable crc (what do?)
		// - Set CE chip enable to enter RX/TX mode respectively
		#if NRFMODE == RECIEVER
			xmit_mode_select(RXMODE);
		#endif
		#if NRFMODE == TRANSMITTER
			xmit_mode_select(TXMODE);
		#endif
		init_RF_flag++;
	}
	else if(init_RF_flag == 4){
		read_register(CONFIG);
		// read_register(EN_RXADDR);
		// read_register(RX_PW_P0);
		// read_register(RF_CH);
		// read_register(RF_SETUP);

		init_RF_flag++;
	}
}
