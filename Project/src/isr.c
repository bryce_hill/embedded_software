#include "isr.h"

// Kick this timer if a PWM channel has been tripped
// TODO - This is currently unused. Don't think we need
void check_isr_flags(void){
  if(my_isr_flags==0){return;}
  else{
    if(my_isr_flags & (0x01<<RED_TOGGLE_FLAG) == 1){toggle_led(RED);}
    if(my_isr_flags & (0x01<<GREEN_TOGGLE_FLAG) == 1){toggle_led(GREEN);}
    if(my_isr_flags & (0x01<<BLUE_TOGGLE_FLAG) == 1){toggle_led(BLUE);}
//    if(my_isr_flags & (0x01<<TX_FLAG) == 1){;}
//    if(my_isr_flags & (0x01<<RX_FLAG) == 1){;}
  }
}

// TODO - Currently unusable, as interrupt never get's us here
void TPM0_IRQHandler(void){
    LOG0("We are in TPM0 Handler");
    //  if(TPM0_STATUS.CH0F==1){
    if((TPM0_STATUS | TPM_STATUS_CH0F_MASK)>>TPM_STATUS_CH0F_SHIFT == 1){
    my_isr_flags |= (0x01<<RED_TOGGLE_FLAG);
    TPM0_STATUS &= TPM_STATUS_CH0F_MASK;
    }
    if((TPM0_STATUS | TPM_STATUS_CH1F_MASK)>>TPM_STATUS_CH1F_SHIFT==1){
    my_isr_flags|=(0x01<<GREEN_TOGGLE_FLAG);
    TPM0_STATUS &= TPM_STATUS_CH1F_MASK;
    }
    if((TPM0_STATUS | TPM_STATUS_CH1F_MASK)>>TPM_STATUS_CH1F_SHIFT==1){
    my_isr_flags|=(0x01<<BLUE_TOGGLE_FLAG);
    TPM0_STATUS &= TPM_STATUS_CH2F_MASK;
    }
    /*
    if(UART TX FLAG==1){
    isr_flags|=(0x01<<TX_FLAG);
    }
    if(UART RX FLAG==1){
    isr_flags|=(0x01<<RX_FLAG);
    }
    */
    TPM0_SC |= TPM_SC_TOF_MASK;     // Clear the Timer Overflow Flag (yes, 1 clears it)
    return;
}

void TPM1_IRQHandler(void){
    my_systicks += 0xFFFF;          // 64 bit timer
    TPM1_SC |= TPM_SC_TOF_MASK;     // Clear the Timer Overflow Flag (yes, 1 clears it)
    tpm1Flag++;
//     LOG0("\n* We hit TPM1 Handler *\n\r");
}

void TPM2_IRQHandler(void){
    TPM2_SC |= TPM_SC_TOF_MASK;     // Clear the Timer Overflow Flag (yes, 1 clears it)
    // LOG0("\n* We hit TPM2 Handler *\n\r");
}

void DMA0_IRQHandler(void){
    LOG0("\n* We hit DMA0 Handler *\n\r");
    dma_timer = stop_timer(dma_timer);
    DMA_DSR0 |= DMA_DSR_BCR_DONE_MASK;
    count--;
    return;
}

void SPI0_IRQHandler(void){
    if(SPI0_S & SPI_S_SPRF_MASK){
        spi0_rxflag = 1;
        uint8_t byte;
        byte = SPI0_D + 0x30;
        // LOG0("* We hit SPI0 Handler * = ");
        // LOG8(&byte, 1);
        // LOG0("\n\r");
    }
}
