#include "message.h"

CircBuf_t Instantiate_Struct(CircBuf_t RXbuffer){
	struct CI_Msg_t *msgPtr, msg;
	// bufferPrint(RXbuffer);
	msgPtr = &msg;
	// Get command id
	RXbuffer = bufferPop(RXbuffer, (uint8_t*)&(msgPtr->command), 1);
	// Get length of message
	RXbuffer = bufferPop(RXbuffer, &(msgPtr->length), 1);
	// Get data of message
	RXbuffer = bufferPop(RXbuffer, &(msgPtr->data[0]), msgPtr->length);

	// printf("Command id = %d\n", (uint8_t)msgPtr->command);
	// printf("Data[0] = %d | Length = %d\n", msgPtr->data[0], msgPtr->length);

	// Get checksum
	RXbuffer = bufferPop(RXbuffer, &(msgPtr->checksum), 1);
	// uint8_t *num = RXbuffer.buffCounter;
	// uint8_t s[] = "Size of buffCounter = \n\r";
	// LOG8(s, sizeof(s));
	// LOG8(num, sizeof(num));
	// uint8_t s1[] = "\n\r";
	// LOG8(s1, sizeof(s1));

	Decode_CI_Msg(msgPtr);
	return(RXbuffer);
}

void Decode_CI_Msg(struct CI_Msg_t *msg){
	uint8_t cmd = (uint8_t)msg->command-48;
	if(cmd == (uint8_t)FRDM_LED_CONTROL){
		FRDM_LED_CONTROL_FUNC(msg);
	}
	else if(cmd == (uint8_t)print){
		PRINT_COMMAND(msg);
	}
	else{
		uint8_t m[] = "Unrecognized Command\n\r";
		LOG8(m, sizeof(m));
	}
}

void PRINT_COMMAND(struct CI_Msg_t *msg){
	uint8_t header[] = "Arrived in print command, printing data:\r\n";
	uint8_t headerL[] = "Length: ";
	uint8_t nl[] = "\n\r";
	uint8_t headerD[] = "Data = ";
	LOG8(header, sizeof(header));
	LOG8(headerL, sizeof(headerL));

	uint8_t *l = &(msg->length);
	LOG8(l, 1);
	LOG8(nl, sizeof(nl));

	LOG8(headerD, sizeof(headerD));
	uint8_t *data = &(msg->data[0]);
	uint8_t size = msg->length;
	LOG8(data, size);
}

void FRDM_LED_CONTROL_FUNC(struct CI_Msg_t *msg){
	// printf("Data[0]  = %d and Data[1] = %d\n", msg->data[0], msg->data[1]);
	uint8_t color = msg->data[0]-48;
	uint8_t action = msg->data[1]-48;
	// RED LED Operations:
	if(color == (uint8_t)RED){
		if(action == (uint8_t)ON){
			#if arch == frdm
				led_on(RED);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_on(RED)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)OFF){
			#if arch == frdm
				led_off(RED);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_off(RED)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)TOGGLE){
			#if arch == frdm
				toggle_led(RED);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_toggle(RED)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
	}
	// GREEN LED Operations:
	else if(color == (uint8_t)GREEN){
		if(action == (uint8_t)ON){
			#if arch == frdm
				led_on(GREEN);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_on(GREEN)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)OFF){
			#if arch == frdm
				led_off(GREEN);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_off(GREEN)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)TOGGLE){
			#if arch == frdm
				toggle_led(GREEN);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_toggle(GREEN)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
	}
	// BLUE LED Operations:
	else if(color == (uint8_t)BLUE){
		if(action == (uint8_t)ON){
			#if arch == frdm
				led_on(BLUE);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_on(BLUE)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)OFF){
			#if arch == frdm
				led_off(BLUE);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_off(BLUE)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
		else if(action == (uint8_t)TOGGLE){
			#if arch == frdm
				toggle_led(BLUE);
			#endif
			#if DEBUG == ON
				uint8_t m[] = "CMD: led_toggle(BLUE)\n\r";
				LOG8(m, sizeof(m));
			#endif
		}
	}
	else{
		uint8_t m1[] = "Uninterpretable Data\n\r";
		LOG8(m1, sizeof(m1));
	}
}

void cmd_task(void){
	#if arch == frdm
		#if MESSAGES == KEYLED
			user_input_pwm_led();
		#endif

		#if MESSAGES == CMDS
			// TODO fix this > 3 issue - now have global vars working, need checksum implemented, but where? In ISR?
			if(RXbuffer.buffCounter > 3 ){
				RXbuffer = Instantiate_Struct(RXbuffer);
			}
		#endif
	#endif
}
