#include "project_2.h"


void my_project_2_report(void){
	itoaUnitTest();
	ftoaUnitTest();
	circBufUnitTest();
}

void itoaUnitTest(void){
	#if arch == host
		printf("\n ------ Project 2 Output: ------ \n");
		int32_t data = 10007;
		int8_t result[5] = {0};
		int8_t* res = &result[0];
		int8_t numRes = 0;
		numRes = my_itoa( res, data);

		// Print our result for verification purposes
		int j = 0;
		printf("Result (itoA): ");
		for(j = 0; j< numRes; j++){
			printf("%d ", result[j]);
		}
		printf("\n\n");
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	#endif
}

void ftoaUnitTest(void){
	#if arch == host
		printf("\n ------ Project 2 Output: ------ \n");
		float data = 10007.7;
		int32_t result[38] = {0};
		int8_t precision = 11;
		int32_t* res = result;

		res = my_ftoa(data, res, precision);

		// Print our result for verification purposes
		int j = 0;
		printf("Result (ASCII): ");
		for(j = 0; j<=11; j++){
			printf("%d ", result[j]);
		}
		printf("\n\n");
		printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	#endif
}

void circBufUnitTest(void){
	#if arch == host
		printf(" ------ Circular Buffer Test -----\n");
		// Buffer unit test
		CircBuf_t buffer;
		buffer.buffCounter = 0;
		printf("Buffer 1: \n");
		buffer = createBuffer(buffer,128);
		uint8_t size = 10;
		// bufferstates_t status;// = bufferStatus(buffer);
		// Write to buffer
		int8_t src[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11};
		int8_t * srcPtr;
		srcPtr = &src[0];
		buffer = bufferPush(buffer, srcPtr, size);
		bufferPrint(buffer);


		// Buffer overflow/wrap unit test
		CircBuf_t buffer2;
		buffer2.buffCounter = 0;
		printf("Buffer 2: \n");
		buffer2 = createBuffer(buffer2,4);
		int8_t src2[5] = {11, 22, 33, 44, 55};
		int8_t* src2ptr = src2;
		int8_t size2 = 5;
		// bufferstates_t status2;// = bufferStatus(buffer2);
		buffer2 = bufferPush(buffer2, src2ptr, size2);
		bufferLog(buffer2);
		bufferPrint(buffer2);

		// Buffer remove/add unit test
		CircBuf_t bufferAddRm;
		bufferAddRm.buffCounter = 0;
		int8_t src3[] = {12, 5, 38};
		int8_t* src3ptr = src3;
		int8_t size3 = 3;
		bufferAddRm = createBuffer(bufferAddRm, size3);
		bufferAddRm = bufferPush(bufferAddRm, src3ptr, size3);
		bufferPrint(bufferAddRm);
		int8_t dest = 0;
		int8_t* destPtr = &dest;
		bufferAddRm = bufferPop(bufferAddRm, destPtr, 1);
		int8_t t[] = {15};
		int8_t* tptr = t;
		// bufferPrint(bufferAddRm);
		bufferAddRm = bufferPush(bufferAddRm, tptr, 1);
		bufferPrint(bufferAddRm);

		// Buffer remove/add unit test 2
		CircBuf_t bufferAddRm2;
		bufferAddRm2.buffCounter = 0;
		int8_t src4[] = {11};
		int8_t* src4ptr = src4;
		int8_t size4 = 30;
		bufferAddRm2 = createBuffer(bufferAddRm2, size4);
		bufferAddRm2 = bufferPush(bufferAddRm2, src4ptr, 1);
		bufferPrint(bufferAddRm2);
		int8_t dest2 = 0;
		int8_t* dest2Ptr = &dest2;
		bufferAddRm2 = bufferPop(bufferAddRm2, dest2Ptr, 1);
		bufferPrint(bufferAddRm2);
		printf("dest2 = %d\n", dest2);

		int8_t t2[] = {22};
		int8_t* t2ptr = t2;
		bufferAddRm2 = bufferPush(bufferAddRm2, t2ptr, 1);
		bufferAddRm2 = bufferPop(bufferAddRm2, dest2Ptr, 1);
		bufferPrint(bufferAddRm2);
		printf("dest2 = %d\n", dest2);


	#endif
}

uint8_t echo_1(void){
	uint8_t msg;
	#if arch == frdm
		if (poll_uart0()==FULL){
			msg = get_uart0();
			//put_uart0(msg);
			return(0);
		}
	#endif
	return(1);
}

uint8_t echo_3(void){
	uint8_t msg;
	#if arch == frdm
		if (poll_uart0()==1){
			msg = get_uart0();
			msg += 3;
			//put_uart0(msg);
			return(1);
		}
	#endif
	return(0);
}
