// #include "main.h"
#include "project_1.h"


void my_project_1_report(void){
  printf("\nMemory manipulation demonstration: \n\n");
  // part i) Create the array
  uint8_t array[32];

  // part ii) Initialize the array pointers
  uint8_t * ptr1 = &array[0];
  uint8_t * ptr2 = &array[8];
  uint8_t * ptr3 = &array[16];
  uint8_t i;

  // part iii) Initialize the array memory
  printf("Initialized array:    ");
  for(i=0;i<(46-31+1);i++){
    *(ptr1+i) = 31+i;
  }
  my_memzero(ptr3,32-17);
  for(i=0;i<sizeof(array);i++){
    printf("%d ",*(ptr1+i));
  }

  // part iv)
  printf("\nMemmove ptr1 to ptr3: ");
  my_memmove(ptr1,ptr3,8);
  for(i=0;i<sizeof(array);i++){
    printf("%d ",*(ptr1+i));
  }

  // part v)
  printf("\nMemmove ptr2 to ptr1: ");
  my_memmove(ptr2,ptr1,16);
  for(i=0;i<sizeof(array);i++){
    printf("%d ",*(ptr1+i));
  }

  // part vi)
  printf("\nReverse entire array: ");
  my_reverse(ptr1,32);
  for(i=0;i<sizeof(array);i++){
    printf("%d ",*(ptr1+i));
  }
  printf("\n\n\n............\n\n\n");

  printf("ASCII to Integer Demonstration: \n\n");
  // demonstrate atoi and itoa functionality
  uint8_t ascii[9] = {'A','S','E','N',' ','5','0','1','3'};
  uint8_t * ptr4 = &ascii[0];
  printf("ASCII: '");
  for(i=0;i<sizeof(ascii);i++){
    printf("%c",*(ptr4+i));
  }
  int32_t integer = my_atoi(ptr4,sizeof(ascii),10);
  printf("'\nInteger: ");
  printf("%d ",integer);
  printf("\n\n\n............\n\n\n");

  printf("Endian conversion demonstration: \n\n");
  // demonstrate my_big_to_little and my_little_to_big functionality
  uint32_t bignum = 0xDEADBEEF;
  printf("Big Endian Number:        %X \n",bignum);
  printf("Little Endian Conversion: %X \n\n",my_big_to_little(bignum));

  uint32_t lilnum = 0x01020304;
  printf("Little Endian Number:     %X \n",lilnum);
  printf("Big endian conversion:    %X \n",my_little_to_big(lilnum));

  printf("\n");
  return;
}
