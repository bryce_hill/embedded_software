#include "init.h"

uint8_t initialize_ARCH(void) {
    // DEFINE & INITIALIZE to 0 the global variables from macro.h (Must also be defined in respective header files)
    uint64_t my_systicks = 0;
    uint8_t tpm1Flag = 0;
    uint64_t count = 0;

    #if arch == frdm
        initialize_uart0();
        initialize_rgbled();
    	timer_init();
		dma_config_0();
		#if SPI == ON
        	initialize_spi0();
            // initialize_nRF24L01();
		#endif
	#endif

    #if arch == bbb

    #endif

    #if DEBUG == 1 // Log what architecture was initialized
        #if arch == host
            LOG0("* Compiled for host architecture, printf used instead of UART *\n");
        #endif

        #if arch == frdm
            LOG0("* Architecture = FRDM board *\n\n\r");
        #endif

        #if arch == bbb
            LOG0("* Compiled for BBB architecture, printf used instead of UART *\n");
        #endif
    #endif
	return(SUCCESS);
}
